def evaluate_realtime_image(img_dir, save_figure_addr_num, figure_num, logs_train_dir):
    max_index=[]
    for img_name in range(save_figure_addr_num, figure_num):
        img_path = img_dir + str(img_name) + '.jpg'
        img = Image.open(img_path)
        # plt.imshow(img)
        imag = img.resize([64, 64])  # 由于图片在预处理阶段以及resize，因此该命令可略
        image_array = np.array(imag)
        # image_array = image
        with tf.Graph().as_default():
            BATCH_SIZE = 1
            N_CLASSES = 3

            image = tf.cast(image_array, tf.float32)
            # image = tf.image.per_image_standardization(image)
            image = tf.reshape(image, [1, 64, 64, 1])

            logit = re_5step_model.inference(image, BATCH_SIZE, N_CLASSES)
            logit = tf.nn.softmax(logit)
            x = tf.placeholder(tf.float32, shape=[64, 64])

            # you need to change the directories to yours.
            # logs_train_dir = 'C:/Users/hao/Desktop/cnn_train/model_weight/gray_dpi0_nunaxi3'
            # logs_train_dir = 'C:/Users/hao/Desktop/cnn_train/model_weight/gray_dpi400'
            saver = tf.train.Saver()

            with tf.Session() as sess:
                # print("Reading checkpoints...")
                ckpt = tf.train.get_checkpoint_state(logs_train_dir)
                if ckpt and ckpt.model_checkpoint_path:
                    # global_step = ckpt.model_checkpoint_path.split('/')[-1].split('-')[-1]
                    saver.restore(sess, ckpt.model_checkpoint_path)
                    # print('Loading success, global_step is %s' % global_step)

                prediction = sess.run(logit, feed_dict={x: image_array})
                max_index.append(np.argmax(prediction))
                # results.append(max_index)
                # print('max_index ', flag, '  :', max_index)
                # flag = flag + 1
                # if max_index == 0:
                #     # print('This is a S with possibility %.6f' % prediction[:, 0])
                #     # load_S.append(status_on)
                #     print('load: S', 'on:', status_on, 'off:', status_off)
                # elif max_index == 1:
                #     # print('This is a O with possibility %.6f' % prediction[:, 1])
                #     # load_O.append(status_on)
                #     print('load: O', 'on:', status_on, 'off:', status_off)
                # elif max_index == 2:
                #     # print('This is a M with possibility %.6f' % prediction[:, 2])
                #     # load_M.append(status_on)
                #     print('load: M', 'on:', status_on, 'off:', status_off)

    return max_index