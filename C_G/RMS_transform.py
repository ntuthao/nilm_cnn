
import numpy as np

def RMS_transform(voltage,current):

    Fs=1920
    Vsquare=np.power(voltage,2)
    DataEnd = len(Vsquare);
    YY=int(DataEnd/Fs)
    x = 0

    Integral1=[]
    for y in range(1,YY+1):
        Integral1.append(np.sum(Vsquare[x:x+Fs])/Fs)
        x = x + Fs
    RMS_voltage=np.power(Integral1,0.5)

    Csquare=np.power(current,2)
    DataEnd = len(Csquare);
    YY=int(DataEnd/Fs)
    x = 0

    Integral1=[]
    for y in range(1,YY+1):
        Integral1.append(np.sum(Csquare[x:x+Fs])/Fs)
        x = x + Fs
    RMS_current=np.power(Integral1,0.5)

    Power = np.multiply(RMS_voltage,RMS_current)

    return Power