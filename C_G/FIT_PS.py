import numpy as np
import matplotlib.pyplot as plt

def fit_ps(voltage,current):

    SampPerPeriod=32
    zero=[]
    z_shift=[]
    Len=[]
    Dis=[]

    for i in range(0,len(voltage)-1):
        if voltage[i+1]>0 and voltage[i]<0:
            zero.append(i)
        elif voltage[i+1]>0 and voltage[i]==0 and voltage[i-1]<0:
            zero.append(i)

    # print(len(zero))
    if len(zero)<60:  # 固定 1888個取樣點。才可以電流分離
        zero.append(1918)
    elif len(zero)>60:
        del zero[60]

    # print(len(zero),zero)

    for i in range(0,len(zero)):
        shift = -voltage[zero[i]] / (voltage[zero[i]+1]-voltage[zero[i]])
        z_shift.append(shift)

    k1 = [[0 for _ in range(32)] for _ in range(len(z_shift)-1)]
    k2 = [[0 for _ in range(32)] for _ in range(len(z_shift) - 1)]
    k3 = [[0 for _ in range(32)] for _ in range(len(z_shift) - 1)]
    for i in range(0,len(z_shift)-1):
        shift_Len=(zero[i+1]+z_shift[i+1])-(zero[i]+z_shift[i])
        Len.append(shift_Len)
        Dis.append(shift_Len/SampPerPeriod)

        k1[i][0] = zero[i]+z_shift[i]
        k2[i][0] = int(np.floor(k1[i][0]))
        k3[i][0] = int(np.ceil(k1[i][0]))

        for k in range(1,SampPerPeriod):
            k1[i][k] = zero[i] + z_shift[i] + Dis[i]*(k-1)
            k2[i][k] = int(np.floor(k1[i][k]))
            k3[i][k] = int(np.ceil(k1[i][k]))
    # print(k2)
    # print(k3)
    for i in range(0,len(z_shift)-1):
        for k in range(1, SampPerPeriod):
            if k2[i][k]-k2[i][k-1]!=1:
                k2[i][k]=k2[i][k-1]+1
            if k2[i][k]+1<1920:
                k3[i][k] = k2[i][k] + 1
            else:
                k2[i][k]=1919
                k3[i][k] = k2[i][k]

    # print('/////////////////////////////////////////')
    # print(k2)
    # print(k3)

    new_V = [[0 for _ in range(32)] for _ in range(len(z_shift) - 1)]
    new_C = [[0 for _ in range(32)] for _ in range(len(z_shift) - 1)]
    for i in range(0,len(z_shift)-1):
        new_V[i][0] = voltage[k2[i][0]] + (voltage[k3[i][0]] - voltage[k2[i][0]]) * z_shift[i]
        new_C[i][0] = current[k2[i][0]] + (current[k3[i][0]] - current[k2[i][0]]) * z_shift[i]
        for k in range(1,SampPerPeriod):
            new_V[i][k] = voltage[k2[i][k]] + (voltage[k3[i][k]] - voltage[k2[i][k]]) * z_shift[i]
            new_C[i][k] = current[k2[i][k]] + (current[k3[i][k]] - current[k2[i][k]]) * z_shift[i]

    FITPS_voltage=[]
    FITPS_current=[]
    for i in range(0,len(new_V)):
        for k in range(0,SampPerPeriod):
            FITPS_voltage.append(new_V[i][k])
            FITPS_current.append(new_C[i][k])

    return FITPS_voltage,FITPS_current

# if __name__ == '__main__':
#
#     filename_V = 'C:/Users/hao/Desktop/數據/revise_EMeter_SM_0616_1920_V.txt'  # 電壓數據位置
#     filename_C = 'C:/Users/hao/Desktop/數據/revise_EMeter_SM_0616_1920_C.txt'  # 電流數據位置
#
#     f = open(filename_V, 'r')
#     line = f.read()
#     list = line.split('\n')
#     voltage = np.array(list, dtype=float)
#
#     f = open(filename_C, 'r')
#     line = f.read()
#     list = line.split('\n')
#     current = np.array(list, dtype=float)
#
#     i=3

    # V_i_next1, C_i_next1 = fit_ps(voltage[(i + 1) * 1920:(i + 2) * 1920],current[(i + 1) * 1920:(i + 2) * 1920])  # 第 i+1 秒

    # V_i_0, C_i_0 = fit_ps(voltage[(i + 1) * 1920:(i + 2) * 1920], current[(i + 1) * 1920:(i + 2) * 1920])  # +1
    # V_i, C_i = fit_ps(voltage[i * 1920:(i + 1) * 1920], current[i * 1920:(i + 1) * 1920])
    # V_i_1, C_i_1 = fit_ps(voltage[(i - 1) * 1920:i * 1920], current[(i - 1) * 1920:i * 1920])  # -1

    # plt.plot(C_i)
    # plt.plot(current[i * 1920:(i + 1) * 1920])
    # plt.plot(voltage[i * 1920:(i + 1) * 1920])
    # plt.show()

#     V_i_2, C_i_2 = fit_ps(voltage[(i - 3) * 1920:(i - 2) * 1920], current[(i - 3) * 1920:(i - 2) * 1920])
#
#     arr_C_i_0 = np.array(C_i_0)  # +1
#     arr_C_i = np.array(C_i)
#     arr_C_i_1 = np.array(C_i_1)  # -1
#     arr_C_i_2 = np.array(C_i_2)
#
#     temp_arr_0 = arr_C_i_0 - arr_C_i_2
#     temp_arr_1 = arr_C_i - arr_C_i_2
#     temp_arr_2 = arr_C_i_1 - arr_C_i_2
#
#     list_0 = []
#     for loop_temp_arr in range(0, len(temp_arr_0)):
#         list_0.append(temp_arr_0[loop_temp_arr])
#     list_1 = []
#     for loop_temp_arr in range(0, len(temp_arr_1)):
#         list_1.append(temp_arr_1[loop_temp_arr])
#     list_2 = []
#     for loop_temp_arr in range(0, len(temp_arr_2)):
#         list_2.append(temp_arr_2[loop_temp_arr])
#
#     separate_current = list_2 + list_1 + list_0
#
#     threshold = 1.0
#     thd_threshold = 15  # 20->15
#     I_cycle = []
#     THD = 0
#     img_size = 32;
#
#     for loop_cycle in range(0, int(len(separate_current) / 32)):
#         val_to_mean = separate_current[loop_cycle * 32:(loop_cycle + 1) * 32]
#         I_cycle_mean = np.mean(val_to_mean)
#         signal_to_filter = [x - I_cycle_mean for x in
#                             separate_current[loop_cycle * 32:(loop_cycle + 1) * 32]]
#         I_cycle.append(np.sum(np.abs(signal_to_filter)) / 32)
#
#     loop_I_cycle = 0
#     fist_ed = 1
#     while 1:
#         delta_I = I_cycle[loop_I_cycle + 1] - I_cycle[loop_I_cycle]
#         print('delta_I: ', np.abs(delta_I))
#         # if np.abs(delta_I) > threshold:
#         #     print('delta_I: ', np.abs(delta_I))
#         loop_I_cycle = loop_I_cycle + 1
#         if fist_ed > 1 or loop_I_cycle >= len(I_cycle) - 1:
#             break

    # plt.plot(separate_current)
    # plt.show()
    # plt.plot(C_i_2)
    # plt.show()

