import tensorflow as tf
from PIL import Image
# I = Image.open("C:/Users/hao/Desktop/cnn_train/test/dog/d1.jpg")
# L = I.convert('L')
# # L.show()
# L.save("C:/Users/hao/Desktop/cnn_train/test/gray_dog/d1.jpg")

# 删除文件夹下面的所有文件(只删除文件,不删除文件夹)
import os
import shutil
import time
import math
import numpy as np
import matplotlib.pyplot as plt
from scipy.fftpack import fft,ifft

import RMS_transform as rms
from sklearn.preprocessing import MaxAbsScaler
# filename_V = 'C:/Users/hao/Desktop/數據/SM_0420_CT_1920_1_V.txt'  # 電壓數據位置
# filename_C = 'C:/Users/hao/Desktop/數據/SM_0420_CT_1920_1_C.txt'  # 電流數據位置
# f = open(filename_V, 'r')
# line = f.read()
# list = line.split('\n')
# voltage = np.array(list, dtype=float)
#
# f = open(filename_C, 'r')
# line = f.read()
# list = line.split('\n')
# current = np.array(list, dtype=float)
#
# i=85
# plt.plot(current[i * 1920:(i + 1) * 1920])
# plt.show()

# i=0
# for img_name in os.listdir('C:/Users/hao/Desktop/cnn_train/grey/1920/test/'):
#     img_path = 'C:/Users/hao/Desktop/cnn_train/grey/1920/test/' + img_name
#     img = Image.open(img_path)
#     img = img.resize((64, 64))
#     L = img.convert('L')
#     L.save('C:/Users/hao/Desktop/cnn_train/grey/1920/64/test/I/' + str(i) + '.jpg')
#     i=i+1

# a=[-0.4,22,5.9,-57,4.6]
# signal_to_filter = [(x - np.mean(a))/(np.max(a)-np.min(a)) for x in a]


# f = open('C:/Users/hao/Desktop/cnn_train/grey/bay_prob.txt', 'a')
# f.write('\n')
# f.write(str(np.array(a)))
# f.close()
# b=[5,5,5,5,5]
# max_abs_scaler = preprocessing.MaxAbsScaler()
# x_train_maxsbs = max_abs_scaler.fit_transform(x)
# np.savetxt("C:/Users/hao/Desktop/cnn_train/grey/bay_prob.txt", b,fmt='%f',delimiter=',')
# from sklearn.preprocessing import MinMaxScaler
# min_max_scaler = MinMaxScaler(feature_range=(-1,1))
# X_train_minmax = min_max_scaler.fit_transform(x)
# print(X_train_minmax)


import pylab as pl
import scipy.signal as signal

from scipy import fftpack
import random

from matplotlib.font_manager import FontManager
import subprocess

# mpl_fonts = set(f.name for f in FontManager().ttflist)
#
# print('all font list get from matplotlib.font_manager:')
# for f in sorted(mpl_fonts):
#     print('\t' + f)

# plt.rcParams['font.sans-serif']=['SimHei'] #用来正常显示中文标签
# plt.rcParams['axes.unicode_minus']=False #用来正常显示负号
# import matplotlib
# matplotlib.rc("font",family='YouYuan')
#
# filename = 'C:/Users/hao/Desktop/cnn_train/grey/temp_zone/train_ED/paper_data/ED_derivativeair.txt'  # 電壓數據位置
# f = open(filename, 'r')
# line = f.read()
# list = line.split('\n')
# one_derivative = np.array(list, dtype=float)
#
# derivative_2D = [[0 for _ in range(len(one_derivative))] for _ in range(len(one_derivative))]
#
# for loop_row in range(0, len(one_derivative)):
#     for loop_column in range(0, len(one_derivative)):
#         derivative_2D[loop_row][loop_column] = one_derivative[loop_column]
#
# plt.imshow(derivative_2D)
# plt.xticks([])
# plt.yticks([])
# plt.title('greyscale')
# plt.savefig('C:/Users/hao/Desktop/cnn_train/grey/temp_zone/train_ED/paper_data/air2D' + '.jpg')
# plt.close()
# img = Image.open('C:/Users/hao/Desktop/cnn_train/grey/temp_zone/train_ED/paper_data/air2D' + '.jpg')
# L = img.convert('L')
# L.save('C:/Users/hao/Desktop/cnn_train/grey/temp_zone/train_ED/paper_data/air' + '.jpg')

# derivative_2D = [[0 for _ in range(len(one_derivatives))] for _ in range(len(one_derivatives))]
#
# for loop_row in range(0, len(one_derivatives)):
#     for loop_column in range(0, len(one_derivatives)):
#         derivative_2D[loop_row][loop_column] = one_derivatives[loop_column]


# plt.imshow(derivative_2D)
# plt.xticks([])
# plt.yticks([])
# plt.show()

# for i in range(0,57):
#     print(random.uniform(-0.1, 0.1))


for img_num in range(0,68):

    img = Image.open('C:/Users/hao/Desktop/cnn_train/grey/1920/ED/train/T/' + str(img_num) + '.jpg')
    img = img.resize((64, 64))
    img.save('C:/Users/hao/Desktop/cnn_train/grey/1920/ED/train/64/RGB/T/' + str(img_num) + '.jpg')
    # L = img.convert('L')
    # L.save('C:/Users/hao/Desktop/cnn_train/grey/1920/ED/train/64/F/' + str(img_num) + '.jpg')





def thd(sampled_data):
    abs_yf = np.abs(fft(sampled_data))
    abs_data=abs_yf[1:int(len(abs_yf) / 2)]
    # sq_sum=0.0
    sq_sum=abs_data[179]**2+abs_data[299]**2+abs_data[419]**2
    # for r in range( len(abs_data)):
    #    sq_sum = sq_sum + (abs_data[r])**2
    # sq_harmonics = sq_sum -abs_data[59]**2.0
    sq_harmonics=sq_sum**0.5
    thd = 100*sq_harmonics / abs_data[59]

    return thd