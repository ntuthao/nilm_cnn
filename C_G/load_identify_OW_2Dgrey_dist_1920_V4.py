
# 辨識混和負載(加入FIT-PS)  取樣頻率：1920  特徵：原始波形 、灰階圖像   事件檢測：mean + 補償機制 CNN    歐式距離分類縣性與非線性

#### 無再次檢測電流爬升位置

import numpy as np
import matplotlib.pyplot as plt
import os
from PIL import Image
from scipy.spatial.distance import euclidean
from fastdtw import fastdtw
import tensorflow as tf
from scipy.fftpack import fft ,ifft

import RMS_transform as rms
import RMS_transform_period as rms_period
import del_file as del_file
import CNN_model
import CNN_model_RGB
import FIT_PS
import CUSUM

from sklearn.preprocessing import MinMaxScaler

def run():

    filename_V = 'C:/Users/hao/Desktop/論文數據/air/air1_1013_CT_1920_4_V.txt'  # 電壓數據位置
    filename_C = 'C:/Users/hao/Desktop/論文數據/air/air1_1013_CT_1920_4_C.txt'  # 電流數據位置


    # 論文數據  S_1022_CT_1920_V  pc2_1111_CT_1920_C  M_1022_CT_1920_V
    #          I_1022_CT_1920_V  air1_1013_CT_1920_1_C  H_1022_CT_1920_V


    # S_0522_CT_1920_C I_0513_700w_CT_1920_V

    # S_1012_CT_1920_V

    datebase_mean = 'C:/Users/hao/Desktop/cnn_train/grey/DB_mean.txt'  ## DB_mean_standard180 DB_mean

    f = open(filename_V, 'r')
    line = f.read()
    list = line.split('\n')
    voltage = np.array(list, dtype=float)

    f = open(filename_C, 'r')
    line = f.read()
    list = line.split('\n')
    current = np.array(list, dtype=float)

    f = open(datebase_mean, 'r')
    line = f.read()
    list = line.split('\n')
    DB_mean = np.array(list, dtype=float)

    i = 2;

    current_rms_period_threshold = 0.15
    mean_threshold = 0.7

    save_img_addr = 'C:/Users/hao/Desktop/cnn_train/grey/temp_zone/identify_feature/'
    save_resizeimg_addr = 'C:/Users/hao/Desktop/cnn_train/grey/temp_zone/identify_feature_64/'
    save_EDimg_addr = 'C:/Users/hao/Desktop/cnn_train/grey/temp_zone/CNN_ED/'
    save_EDimg64_addr = 'C:/Users/hao/Desktop/cnn_train/grey/temp_zone/CNN_ED64/'

    del_file.del_file(save_img_addr)
    del_file.del_file(save_resizeimg_addr)
    del_file.del_file(save_EDimg_addr)
    del_file.del_file(save_EDimg64_addr)
    del_file.del_file('C:/Users/hao/Desktop/cnn_train/grey/temp_zone/current_waveform/')

    Power = rms.RMS_transform(voltage, current)

    stander_voltage_arr = []  # 標準電壓
    for loop_max in range(0, int( 5 *1920 / 32)):
        stander_voltage_arr.append(np.max(voltage[loop_max * 32:(loop_max + 1) * 32]))
    stander_voltage =np.mean(stander_voltage_arr)
    print(stander_voltage)

    img_num =0
    CNN_ED_imgnum = 0


    check_ED =[]
    thd_list =[]

    load_S = []
    load_O = []
    load_M = []
    load_H = []
    load_I = []
    load_pc = []
    load_air = []

    status_S = 0
    status_O = 0
    status_M = 0
    status_H = 0
    status_I = 0
    status_pc = 0
    status_air = 0

    check_I_cycle = []
    for loop_temp in range(0,len(Power)):
        check_ED.append(0)


    while 1:

        true_ED=0
        check_ED[i]=0

        pc_off_dontcare = 123

        current_rms_period=rms_period.RMS_transform_period(voltage[i * 1920 - 2*32:(i + 1) * 1920], current[i * 1920 - 2*32:(i + 1) * 1920])  ### 電流週期 rms 值
        current_rms_period_1s = rms_period.RMS_transform_period(voltage[i * 1920:(i + 1) * 1920],current[i * 1920:(i + 1) * 1920])  ### 電流週期 rms 值
        # print(len(current_rms_period))
        fist_current_rms_period = 1
        # print(len(current_rms_period))
        for loop_current_rms_period in range(1,len(current_rms_period)):
            if np.abs(current_rms_period[loop_current_rms_period] - current_rms_period[loop_current_rms_period - 1]) > current_rms_period_threshold and fist_current_rms_period == 1:

                ED_position=loop_current_rms_period - (len(current_rms_period)-len(current_rms_period_1s))

                # print('in rms: ', i)
                #/////////////////////////////////////////////////////////////////////////////////////////////////////////// 二次篩選 mean #

                meanED_voltage=voltage[i * 1920 - 2*32:(i + 1) * 1920]
                meanED_current=current[i * 1920 - 2*32:(i + 1) * 1920]

                zero=[]
                for loop_zero in range(0, len(meanED_voltage) - 1):
                    if meanED_voltage[loop_zero + 1] > 0 and meanED_voltage[loop_zero] < 0:
                        zero.append(loop_zero)
                    elif meanED_voltage[loop_zero + 1] > 0 and meanED_voltage[loop_zero] == 0 and meanED_voltage[loop_zero - 1] < 0:
                        zero.append(loop_zero)

                I_cycle = []
                for loop_cycle in range(0, len(zero)-1):
                    val_to_mean = meanED_current[zero[loop_cycle]:zero[loop_cycle+1]]
                    I_cycle_mean = np.mean(val_to_mean)
                    signal_to_filter = [x - I_cycle_mean for x in
                                        meanED_current[zero[loop_cycle]:zero[loop_cycle+1]]]
                    I_cycle.append(np.sum(np.abs(signal_to_filter)) / len(meanED_current[zero[loop_cycle]:zero[loop_cycle+1]]))

                fist_meanED = 1
                for loop_I_cycle in range(0, len(I_cycle) - 1):
                    delta_I = I_cycle[loop_I_cycle + 1] - I_cycle[loop_I_cycle]
                    if np.abs(delta_I) > mean_threshold and fist_meanED==1:

                        ED_position = loop_I_cycle - (len(current_rms_period)-len(current_rms_period_1s))

                        print('in mean: ',i)
                        true_ED=1
                        check_ED[i] = 1

                        ###//////////////////////////////////////////////////////////////////////////////////////////////////////////////###
                        # if Power[i] - Power[i - 1] < 0 and Power[i - 1] - Power[i - 2] > 0: ### 為了解決電腦關閉前的變動
                        #     true_ED = 0
                        #     check_ED[i] = 0
                        #     print('in mean: ', i, ' ', Power[i] - Power[i - 1], 'not!!!!!!!!!!!')
                        ###//////////////////////////////////////////////////////////////////////////////////////////////////////////////###


                        fist_meanED = 2
                        break



                #///////////////////////////////////////////////////////////////////////////////////////////////////////////  補償機制 CNN #
                if fist_meanED == 1:

                    one_derivative = []
                    for loop_S_diff in range(1, len(current_rms_period)):  # len(current_rms_period) = 59
                        one_derivative.append(current_rms_period[loop_S_diff] - current_rms_period[loop_S_diff - 1])
                        # f = open('C:/Users/hao/Desktop/cnn_train/grey/temp_zone/train_ED/para3.txt', 'a')
                        # f.write(str(current_rms_period[loop_S_diff] - current_rms_period[loop_S_diff - 1]))
                        # f.write('\n')
                        # f.close()

                    plt.plot(one_derivative)
                    plt.savefig('C:/Users/hao/Desktop/cnn_train/grey/temp_zone/current_waveform/one_derivative_' + str(i) + '.jpg')
                    plt.close()

                    derivative_2D = [[0 for _ in range(len(one_derivative))] for _ in range(len(one_derivative))]

                    for loop_row in range(0, len(one_derivative)):
                        for loop_column in range(0, len(one_derivative)):
                            derivative_2D[loop_row][loop_column] = one_derivative[loop_column]

                    plt.imshow(derivative_2D)
                    plt.xticks([])
                    plt.yticks([])
                    plt.savefig(save_EDimg_addr + str(CNN_ED_imgnum) + '_' + str(i) + '.jpg')
                    plt.close()

                    img = Image.open(save_EDimg_addr + str(CNN_ED_imgnum) + '_' + str(i) + '.jpg')
                    img = img.resize((64, 64))
                    # img.save(save_EDimg64_addr + str(CNN_ED_imgnum) + '.jpg')
                    L = img.convert('L')
                    L.save(save_EDimg64_addr + str(CNN_ED_imgnum) + '.jpg')

                    max_index = evaluate_one_image(save_EDimg64_addr + str(CNN_ED_imgnum) + '.jpg',
                                                   'C:/Users/hao/Desktop/cnn_train/model_weight/ED_2D_paperV2_1113',2)  ## ED_2D_paperV2_1113  ED_2D_makedata

                    # print('max_index_ED: ',max_index,' ',i)
                    if max_index==1:
                        true_ED = 1
                        check_ED[i] = 1
                        print('!!!!!!!!!!!!!!! ')
                        print('max_index_ED: ', max_index, ' ', i)

                        ###//////////////////////////////////////////////////////////////////////////////////////////////////////////////###

                        # if i==760:
                        #     print(Power[i] - Power[i - 1],' Power[i]: ',Power[i],Power[i]-Power[i+1])
                        pc_off_dontcare=0
                        if Power[i]-Power[i-1] < -80 :  ##解決電腦關閉可以被辨識出來
                            true_ED = 1
                            check_ED[i] = 1
                            print('in cnn_ED: ', i, ' ', Power[i] - Power[i - 1],' Power[i]: ',Power[i], 'pc pff!!!!!!!!!!!')
                            pc_off_dontcare=1
                            print(pc_off_dontcare)
                        elif Power[i]-Power[i+1] > 80:
                            true_ED = 1
                            check_ED[i] = 1
                            print('in cnn_ED: ', i, ' ', Power[i] - Power[i - 1],' Power[i]: ',Power[i], 'pc pff!!!!!!!!!!!')
                            pc_off_dontcare=1
                            print(pc_off_dontcare)


                        if pc_off_dontcare==0:
                            if np.abs(Power[i] - Power[i - 1]) > 50 or Power[i] - Power[i - 1] < -10:  ### 為了解決電腦運作過程的變動
                                true_ED = 0
                                check_ED[i] = 0
                                print('in cnn_ED: ', i, ' ', Power[i] - Power[i - 1],' Power[i]: ',Power[i], 'not1!!!!!!!!!!!')

                            if Power[i - 1] - Power[i] < 20 and Power[i - 1] - Power[i] > 0 and Power[i] - Power[i + 1] < 20:  ## 往下降
                                true_ED = 0
                                check_ED[i] = 0
                                print('in cnn_ED: ', i, ' ', Power[i] - Power[i - 1],' Power[i]: ',Power[i], 'not2!!!!!!!!!!!')

                        ###//////////////////////////////////////////////////////////////////////////////////////////////////////////////###



                    CNN_ED_imgnum = CNN_ED_imgnum + 1


                fist_current_rms_period = 2
                break

        if true_ED == 1:  ### 有事件發生，進行電流分離

            # turn_on = 0
            if Power[i] - Power[i-1] > 0:
                print('---on---')
                turn_on = 1
                separate_current = seperate_current_on(voltage, current, i)
                toscale_V = voltage[(i + 1) * 1920:(i + 2) * 1920]
                max_voltage = []  # 電壓正規化
                for loop_max in range(0, int(len(toscale_V) / 32)):
                    max_voltage.append(np.max(toscale_V[loop_max * 32:(loop_max + 1) * 32]))
                scale_voltage = stander_voltage / np.mean(max_voltage)

                ##//////////////////////////////////////////////////////////////////////////////////////////////////////////////###


                if pc_off_dontcare==1:  ## 為了電腦關閉
                    print('---on to off---')
                    turn_on = -1
                    separate_current = seperate_current_off(voltage, current, i)
                    toscale_V = voltage[(i - 2) * 1920:(i - 1) * 1920]
                    max_voltage = []  # 電壓正規化
                    for loop_max in range(0, int(len(toscale_V) / 32)):
                        max_voltage.append(np.max(toscale_V[loop_max * 32:(loop_max + 1) * 32]))
                    scale_voltage = stander_voltage / np.mean(max_voltage)
                ##//////////////////////////////////////////////////////////////////////////////////////////////////////////////###

            else:
                print('---off---')
                turn_on = -1
                separate_current = seperate_current_off(voltage, current, i)
                toscale_V = voltage[(i - 2) * 1920:(i - 1) * 1920]
                max_voltage = []  # 電壓正規化
                for loop_max in range(0, int(len(toscale_V) / 32)):
                    max_voltage.append(np.max(toscale_V[loop_max * 32:(loop_max + 1) * 32]))
                scale_voltage = stander_voltage / np.mean(max_voltage)

            plt.plot(current[i * 1920:(i + 1) * 1920])
            plt.savefig('C:/Users/hao/Desktop/cnn_train/grey/temp_zone/current_waveform/current_' + str(i) + 'num' + str(img_num) + '.jpg')
            plt.close()

            print('ED_position: ',ED_position)
            max_index = mix_load(separate_current,  i, ED_position, scale_voltage, img_num, DB_mean)

            print('img_num: ', img_num,'//max_index: ', max_index,'//i: ',i,'//',Power[i] - Power[i - 1])
            print('//////////////////////////////////////////////////////////////////////')
            img_num = img_num + 1

            if max_index == 0:
                if Power[i] - Power[i - 1] > 0:
                    status_S = 1
                else:
                    status_S = 0
            elif max_index == 1:

                if Power[i] - Power[i - 1] > 0:
                    status_O = 1
                else:
                    status_O = 0
            elif max_index == 2:
                # load_M.append(status_on)
                if Power[i] - Power[i - 1] > 0:
                    status_H = 1
                else:
                    status_H = 0
            elif max_index == 3:
                if Power[i] - Power[i - 1] > 0:
                    status_M = 1
                else:
                    status_M = 0
            elif max_index == 4:
                if turn_on==1:
                    status_pc = 1
                elif turn_on==-1:
                    status_pc = 0
            elif max_index == 5:
                if Power[i] - Power[i - 1] > 0:
                    status_I = 1
                else:
                    status_I = 0
            elif max_index == 6:
                if Power[i] - Power[i - 1] > 0:
                    status_air = 1
                else:
                    status_air = 0
            # plt.plot(separate_current)
            # plt.savefig(save_EDimg_addr + str(CNN_ED_imgnum) + '_separate_current_' + str(i) + '.jpg')
            # plt.close()


        if (i + 1) * 1920 >= len(current) - 1920 *2:

            fig_1 = 4;
            plt.figure(1)

            plt.subplot(fig_1, 1, 1)
            plt.ylabel('current(A)')
            plt.plot(current)
            plt.subplot(fig_1, 1, 2)
            plt.ylabel('S')
            plt.plot(load_S)
            plt.subplot(fig_1, 1, 3)
            plt.ylabel('O')
            plt.plot(load_O)
            plt.subplot(fig_1, 1, 4)
            plt.ylabel('H')
            plt.plot(load_H)

            # plt.show()

            # plt.xlabel('time(S)')

            fig_2 = 4
            plt.figure(2)

            plt.subplot(fig_2, 1, 1)
            plt.ylabel('M')
            plt.plot(load_M)
            plt.subplot(fig_2, 1, 2)
            plt.ylabel('I')
            plt.plot(load_I)
            plt.subplot(fig_2, 1, 3)
            plt.ylabel('pc')
            plt.plot(load_pc)
            plt.subplot(fig_2, 1, 4)
            plt.ylabel('air')
            plt.xlabel('time(S)')
            plt.plot(load_air)
            # plt.savefig('C:/Users/hao/Desktop/cnn_train/grey/temp_zone/result/S.jpg')
            # plt.subplot_tool()
            # plt.show()

            plt.figure(3)
            plt.subplot(2, 1, 1)
            plt.plot(current)
            plt.subplot(2, 1, 2)
            plt.plot(check_ED)
            plt.show()
            break

        else:
            load_S.append(status_S)
            load_O.append(status_O)
            load_M.append(status_M)
            load_H.append(status_H)
            load_I.append(status_I)
            load_pc.append(status_pc)
            load_air.append(status_air)
            i = i + 1


def mix_load(separate_current, i, ED_position, scale_voltage, img_num, DB_mean):

    dist_threshold = 1 # 歐氏距離
    compwave_threshold = 3  # 比對線性與非線性補償機制

    threshold = 0.5
    rms_threshold = 0.09

    img_size = 32;

    plt.plot(separate_current)
    plt.savefig('C:/Users/hao/Desktop/cnn_train/grey/temp_zone/current_waveform/separate_current_' + str(i) + 'num' + str(img_num) + '.jpg')
    plt.close()

    to_linear = separate_current[(ED_position + 59 + 4) * 32:(ED_position + 59 + 4) * 32 + img_size * 10]
    plt.plot(to_linear)
    plt.savefig('C:/Users/hao/Desktop/cnn_train/grey/temp_zone/current_waveform/to_linear_' + str(i) + 'num' + str(img_num) + '.jpg')
    plt.close()

    array_to_linear = np.array(to_linear)
    array_to_linear = array_to_linear.reshape(array_to_linear.shape[0], 1)

    min_max_scaler = MinMaxScaler(feature_range=(-1, 1))
    nor_to_linear = min_max_scaler.fit_transform(array_to_linear)

    nor_to_linear = nor_to_linear.reshape(1, 320)
    # print(np.array(nor_to_linear) - np.array(DB_mean))

    dist = np.sqrt(np.sum(np.square(np.array(nor_to_linear) - np.array(DB_mean))))
    print("dist : ", dist)

    if dist < dist_threshold:
        linear = 1
    else:
        linear = 0


    if linear == 1:  # 線性

        img_C = separate_current[(ED_position + 59 + 4) * 32:(ED_position + 59 + 4) * 32 + img_size * 10]

        plt.plot(img_C)
        plt.savefig('C:/Users/hao/Desktop/cnn_train/grey/temp_zone/current_waveform/linear_' + str(i) + 'num' + str(
            img_num) + '.jpg')
        plt.close()

        # new_img = []
        # for loop_img_C in range(1, len(img_C)):
        #     if img_C[loop_img_C] > 0 and img_C[loop_img_C - 1] < 0:
        #         for loop_scale in range(0, len(img_C)):
        #             if loop_scale < len(img_C) - loop_img_C:
        #                 new_img.append(img_C[loop_scale + loop_img_C])
        #             else:
        #                 new_img.append(0)
        #         break
        # print('new_img: ', len(new_img))
        img_C = [x * scale_voltage for x in img_C]
        plt.plot(img_C)
        plt.ylim((-20, 20))
        save_img_addr = 'C:/Users/hao/Desktop/cnn_train/grey/temp_zone/identify_feature/'
        plt.savefig(save_img_addr + str(img_num) + '.jpg')
        plt.close()

        save_resizeimg_addr = 'C:/Users/hao/Desktop/cnn_train/grey/temp_zone/identify_feature_64/'
        img = Image.open(save_img_addr + str(img_num) + '.jpg')
        img = img.resize((64, 64))
        L = img.convert('L')
        L.save(save_resizeimg_addr + str(img_num) + '.jpg')

        max_index = evaluate_one_image(save_resizeimg_addr + str(img_num) + '.jpg',
                                       'C:/Users/hao/Desktop/cnn_train/model_weight/OW_1920_SOH_paperV1', 3)
    else:  # 非線性

        img_C = separate_current[(ED_position + 59 + 2) * 32:(ED_position + 59 + 2) * 32 + img_size ** 2]
        img_C = [x * scale_voltage for x in img_C]


        plt.plot(img_C)
        plt.savefig(
            'C:/Users/hao/Desktop/cnn_train/grey/temp_zone/current_waveform/nonlinear_' + str(img_num) + '.jpg')
        plt.close()

        img_C = [x * scale_voltage for x in img_C]
        img_2D = [[0 for _ in range(img_size)] for _ in range(img_size)]
        for row in range(0, img_size):
            for col in range(0, img_size):
                img_2D[row][col] = img_C[row * img_size + col]

        save_img_addr = 'C:/Users/hao/Desktop/cnn_train/grey/temp_zone/identify_feature/'
        plt.imshow(img_2D)
        plt.xticks([])
        plt.yticks([])
        plt.savefig(save_img_addr + str(img_num) + '.jpg')
        plt.close()

        save_resizeimg_addr = 'C:/Users/hao/Desktop/cnn_train/grey/temp_zone/identify_feature_64/'
        img = Image.open(save_img_addr + str(img_num) + '.jpg')
        img = img.resize((64, 64))
        L = img.convert('L')
        L.save(save_resizeimg_addr + str(img_num) + '.jpg')
        # img.save(save_resizeimg_addr + str(img_num) + '.jpg')


        max_index = evaluate_one_image(save_resizeimg_addr + str(img_num) + '.jpg',
                                               'C:/Users/hao/Desktop/cnn_train/model_weight/2Dgrey_1920_MpcIair_paperV6_1112',
                                               4)
        print(max_index)
        max_index = max_index + 3

        # new_img = []
        # start_zero = 1
        # while 1:
        #     if img_C[start_zero] > 0 and img_C[start_zero - 1] < 0:
        #         for loop_scale in range(0, len(img_C)):
        #             if loop_scale < len(img_C) - start_zero:
        #                 new_img.append(img_C[loop_scale + start_zero])
        #             else:
        #                 new_img.append(0)
        #         img_2D = [[0 for _ in range(img_size)] for _ in range(img_size)]
        #         for row in range(0, img_size):
        #             for col in range(0, img_size):
        #                 img_2D[row][col] = new_img[row * img_size + col]
        #         plt.imshow(img_2D)
        #         plt.xticks([])
        #         plt.yticks([])
        #         save_img_addr = 'C:/Users/hao/Desktop/cnn_train/grey/temp_zone/identify_feature/'
        #         plt.savefig(save_img_addr + str(img_num) + '.jpg')
        #         plt.close()
        #
        #         save_resizeimg_addr = 'C:/Users/hao/Desktop/cnn_train/grey/temp_zone/identify_feature_64/'
        #         img = Image.open(save_img_addr + str(img_num) + '.jpg')
        #         img = img.resize((64, 64))
        #         L = img.convert('L')
        #         L.save(save_resizeimg_addr + str(img_num) + '.jpg')
        #
        #         max_index = evaluate_one_image(save_resizeimg_addr + str(img_num) + '.jpg',
        #                                        'C:/Users/hao/Desktop/cnn_train/model_weight/2Dgrey_1920_MpcI700air_paperV4',
        #                                        4)
        #         print(max_index)
        #         max_index = max_index + 3
        #
        #         break
        #     else:
        #         start_zero = start_zero + 1


    return max_index

def seperate_current_on(voltage ,current ,i):
    V_i_0, C_i_0 = FIT_PS.fit_ps(voltage[(i + 1) * 1920:(i + 2) * 1920], current[(i + 1) * 1920:(i + 2) * 1920])  # +1
    V_i, C_i = FIT_PS.fit_ps(voltage[i * 1920:(i + 1) * 1920], current[i * 1920:(i + 1) * 1920])
    V_i_1, C_i_1 = FIT_PS.fit_ps(voltage[(i - 1) * 1920:i * 1920], current[(i - 1) * 1920:i * 1920])  # -1

    V_i_2, C_i_2 = FIT_PS.fit_ps(voltage[(i - 3) * 1920:(i - 2) * 1920], current[(i - 3) * 1920:(i - 2) * 1920])

    arr_C_i_0 = np.array(C_i_0)  # +1
    arr_C_i = np.array(C_i)
    arr_C_i_1 = np.array(C_i_1)  # -1
    arr_C_i_2 = np.array(C_i_2)

    temp_arr_0 = arr_C_i_0 - arr_C_i_2
    temp_arr_1 = arr_C_i - arr_C_i_2
    temp_arr_2 = arr_C_i_1 - arr_C_i_2

    list_0 = []
    for loop_temp_arr in range(0, len(temp_arr_0)):
        list_0.append(temp_arr_0[loop_temp_arr])
    list_1 = []
    for loop_temp_arr in range(0, len(temp_arr_1)):
        list_1.append(temp_arr_1[loop_temp_arr])
    list_2 = []
    for loop_temp_arr in range(0, len(temp_arr_2)):
        list_2.append(temp_arr_2[loop_temp_arr])

    separate_current = list_2 + list_1 + list_0
    return separate_current

def seperate_current_off(voltage ,current ,i):
    V_i_0, C_i_0 = FIT_PS.fit_ps(voltage[(i + 1) * 1920:(i + 2) * 1920], current[(i + 1) * 1920:(i + 2) * 1920])
    V_i, C_i = FIT_PS.fit_ps(voltage[i * 1920:(i + 1) * 1920], current[i * 1920:(i + 1) * 1920])
    V_i_1, C_i_1 = FIT_PS.fit_ps(voltage[(i - 1) * 1920:i * 1920], current[(i - 1) * 1920:i * 1920])

    V_i_2, C_i_2 = FIT_PS.fit_ps(voltage[(i - 3) * 1920:(i - 2) * 1920], current[(i - 3) * 1920:(i - 2) * 1920])

    arr_C_i_0 = np.array(C_i_0)  # +1
    arr_C_i = np.array(C_i)
    arr_C_i_1 = np.array(C_i_1)  # -1
    arr_C_i_2 = np.array(C_i_2)

    temp_arr_0 = arr_C_i_2 - arr_C_i_0
    temp_arr_1 = arr_C_i_2 - arr_C_i
    temp_arr_2 = arr_C_i_2 - arr_C_i_1

    list_0 = []
    for loop_temp_arr in range(0, len(temp_arr_0)):
        list_0.append(temp_arr_0[loop_temp_arr])
    list_1 = []
    for loop_temp_arr in range(0, len(temp_arr_1)):
        list_1.append(temp_arr_1[loop_temp_arr])
    list_2 = []
    for loop_temp_arr in range(0, len(temp_arr_2)):
        list_2.append(temp_arr_2[loop_temp_arr])

    separate_current = list_2 + list_1 + list_0
    return separate_current


# def DTW(voltage,current):
#     DTW_distance=[]
#     zero_points=[]
#     for loop_cycle in range(0,len(current)-1):  # 電壓週期 1秒
#         if voltage[loop_cycle + 1] > 0 and voltage[loop_cycle] < 0:
#             zero_points.append(loop_cycle)
#
#     for loop_zero_points in range(0, len(zero_points)-2):
#         I_cycle1=current[zero_points[loop_zero_points]:zero_points[loop_zero_points + 1]]  ## 第一個週期
#         I_cycle2=current[zero_points[loop_zero_points+1]:zero_points[loop_zero_points + 2]]  ## 第二個週期
#         distance, path = fastdtw(I_cycle1, I_cycle2, dist=euclidean)
#         DTW_distance.append(distance)
#
#     std_DTW=np.std(DTW_distance, ddof=1)
#     # for loop_DTW_distance in range(0,len(DTW_distance)):
#
#     return std_DTW
def DTW(current):
    DTW_distance =[]
    zero_points =[]

    for loop_max in range(0, int(len(current ) /32 ) -1):
        I_cycle1 =current[loop_max * 32:(loop_max + 1) * 32]  ## 第一個週期
        I_cycle2 =current[(loop_max + 1) * 32:(loop_max + 2) * 32]  ## 第二個週期
        distance, path = fastdtw(I_cycle1, I_cycle2, dist=euclidean)
        DTW_distance.append(distance)

    std_DTW =np.std(DTW_distance, ddof=1)

    return std_DTW

def evaluate_one_image(img_dir ,logs_train_dir ,N_CLASSES):

    # logs_train_dir='C:/Users/hao/Desktop/cnn_train/model_weight/ZV_DTW_SO'

    for img_name in range(0 ,1):
        img = Image.open(img_dir)
        imag = img.resize([64, 64])  # 由于图片在预处理阶段以及resize，因此该命令可略
        image_array = np.array(imag)
        # image_array = image
        with tf.Graph().as_default():
            BATCH_SIZE = 1
            # N_CLASSES = 3

            image = tf.cast(image_array, tf.float32)
            # image = tf.image.per_image_standardization(image)
            image = tf.reshape(image, [1, 64, 64, 1])
            # image = tf.reshape(image, [1, 64, 64, 3])

            logit = CNN_model.inference(image, BATCH_SIZE, N_CLASSES)
            logit = tf.nn.softmax(logit)
            x = tf.placeholder(tf.float32, shape=[64, 64])
            # x = tf.placeholder(tf.float32, shape=[64, 64, 3])
            saver = tf.train.Saver()

            with tf.Session() as sess:
                ckpt = tf.train.get_checkpoint_state(logs_train_dir)
                if ckpt and ckpt.model_checkpoint_path:
                    saver.restore(sess, ckpt.model_checkpoint_path)

                prediction = sess.run(logit, feed_dict={x: image_array})
                max_index =np.argmax(prediction)
    return  max_index

def evaluate_one_image_RGB(img_dir ,logs_train_dir ,N_CLASSES):

    # logs_train_dir='C:/Users/hao/Desktop/cnn_train/model_weight/ZV_DTW_SO'

    for img_name in range(0 ,1):
        img = Image.open(img_dir)
        imag = img.resize([64, 64])  # 由于图片在预处理阶段以及resize，因此该命令可略
        image_array = np.array(imag)
        # image_array = image
        with tf.Graph().as_default():
            BATCH_SIZE = 1
            # N_CLASSES = 3

            image = tf.cast(image_array, tf.float32)
            # image = tf.image.per_image_standardization(image)
            # image = tf.reshape(image, [1, 64, 64, 1])
            image = tf.reshape(image, [1, 64, 64, 3])

            logit = CNN_model_RGB.inference(image, BATCH_SIZE, N_CLASSES)
            logit = tf.nn.softmax(logit)
            # x = tf.placeholder(tf.float32, shape=[64, 64])
            x = tf.placeholder(tf.float32, shape=[64, 64, 3])
            saver = tf.train.Saver()

            with tf.Session() as sess:
                ckpt = tf.train.get_checkpoint_state(logs_train_dir)
                if ckpt and ckpt.model_checkpoint_path:
                    saver.restore(sess, ckpt.model_checkpoint_path)

                prediction = sess.run(logit, feed_dict={x: image_array})
                max_index =np.argmax(prediction)
    return  max_index



def thd(sampled_data):
    abs_yf = np.abs(fft(sampled_data))
    abs_data =abs_yf[1:int(len(abs_yf) / 2)]
    # sq_sum=0.0
    # y = sorted(abs_data, reverse=True)
    sq_sum =abs_data[179 ]** 2 +abs_data[299 ]** 2 +abs_data[419 ]**2
    # sq_sum = y[1] ** 2 + y[2] ** 2 + y[3] ** 2
    # for r in range( len(abs_data)):
    #    sq_sum = sq_sum + (abs_data[r])**2
    # sq_harmonics = sq_sum -abs_data[59]**2.0
    sq_harmonics =sq_sum**0.5
    thd = 100 *sq_harmonics / abs_data[59]

    return thd



if __name__ == '__main__':
    run()