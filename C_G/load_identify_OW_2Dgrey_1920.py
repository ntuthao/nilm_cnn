

# 辨識混和負載(加入FIT-PS)  取樣頻率：1920  特徵：原始波形 、灰階圖像   事件檢測：mean  電壓過零點抓一個電流週期  DTW判斷穩態 +THD

import numpy as np
import matplotlib.pyplot as plt
import os
from PIL import Image
from scipy.spatial.distance import euclidean
from fastdtw import fastdtw
import tensorflow as tf
from scipy.fftpack import fft,ifft

import RMS_transform as rms
import del_file as del_file
import CNN_model
import FIT_PS

def run():

    filename_V = 'C:/Users/hao/Desktop/數據/mix1_0527_CT_1920_V.txt'  # 電壓數據位置
    filename_C = 'C:/Users/hao/Desktop/數據/mix1_0527_CT_1920_C.txt'  # 電流數據位置

    # filename_V = 'C:/Users/hao/Desktop/數據/I_0415_500w_CT_V.txt'  # 電壓數據位置
    # filename_C = 'C:/Users/hao/Desktop/數據/I_0415_500w_CT_C.txt'  # 電流數據位置

    # I_0415_500w   O_0412  H_0409_CT   0412  I_0417_  為訓練數據  0415 0416為測試數據

    # filename_V = 'C:/Users/hao/Desktop/數據/mix1_0525_CT_1920_V.txt'  # 電壓數據位置
    # filename_C = 'C:/Users/hao/Desktop/數據/mix1_0525_CT_1920_C.txt'  # 電流數據位置

    # SM_0420_CT_1920_1_V    mix1_0519_CT_1920_V(錯誤)  mix1_0525_CT_1920_V  mix1_0527_CT_1920_V  mix2_0527_CT_1920_V  mix3_0527_CT_1920_C
    # Spc_0609_CT_1920_C  Opc_0612_CT_1920_C  Mpc_0612_CT_1920_C  I700pc_0612_CT_1920_C  Hpc_0612_CT_1920_C



    f = open(filename_V, 'r')
    line = f.read()
    list = line.split('\n')
    voltage = np.array(list, dtype=float)

    f = open(filename_C, 'r')
    line = f.read()
    list = line.split('\n')
    current = np.array(list, dtype=float)


    i = 1;

    P_threshold = 70
    threshold = 0.5

    save_img_addr = 'C:/Users/hao/Desktop/cnn_train/grey/mix/'
    save_resizeimg_addr = 'C:/Users/hao/Desktop/cnn_train/grey/64/mix/'
    del_file.del_file(save_img_addr)
    del_file.del_file(save_resizeimg_addr)
    del_file.del_file('C:/Users/hao/Desktop/cnn_train/grey/c/')

    Power = rms.RMS_transform(voltage, current)

    stander_voltage_arr = []  # 標準電壓
    for loop_max in range(0, int(5*1920 / 32)):
        stander_voltage_arr.append(np.max(voltage[loop_max * 32:(loop_max + 1) * 32]))
    stander_voltage=np.mean(stander_voltage_arr)
    print(stander_voltage)

    img_num=0
    fist_ed=1

    check_ED=[]
    thd_list=[]

    load_S = []
    load_O = []
    load_M = []
    load_H = []
    load_I = []
    load_pc = []

    status_S = 0
    status_O = 0
    status_M = 0
    status_H = 0
    status_I = 0
    status_pc = 0

    while 1:

        if np.abs(Power[i] - Power[i-1]) > P_threshold:

            V_i_next1, C_i_next1 = FIT_PS.fit_ps(voltage[(i + 1) * 1920:(i + 2) * 1920], current[(i + 1) * 1920:(i + 2) * 1920])  # 第 i+1 秒
            V_i, C_i = FIT_PS.fit_ps(voltage[i * 1920:(i + 1) * 1920], current[i * 1920:(i + 1) * 1920])  # 第 i 秒
            V_i_pre1, C_i_pre1 = FIT_PS.fit_ps(voltage[(i - 1) * 1920:i * 1920], current[(i - 1) * 1920:i * 1920])  # 第 i-1 秒
            if Power[i] - Power[i-1]<0:
                V_i_pre2, C_i_pre2 = FIT_PS.fit_ps(voltage[(i - 2) * 1920:(i - 1) * 1920],current[(i - 2) * 1920:(i - 1) * 1920])  # 第 i-2 秒
                C_2s = C_i_pre2 + C_i_pre1 + C_i + C_i_next1
            else:
                C_2s = C_i_pre1 + C_i + C_i_next1

            I_cycle = []
            for loop_cycle in range(0, int(len(C_2s) / 32)):
                val_to_mean = C_2s[loop_cycle * 32:(loop_cycle + 1) * 32]
                I_cycle_mean = np.mean(val_to_mean)
                signal_to_filter = [x - I_cycle_mean for x in
                                    C_2s[loop_cycle * 32:(loop_cycle + 1) * 32]]
                I_cycle.append(np.sum(np.abs(signal_to_filter)) / 32)

            fist_ed=1
            for loop_I_cycle in range(0, len(I_cycle) - 1):
                delta_I = I_cycle[loop_I_cycle + 1] - I_cycle[loop_I_cycle]
                if np.abs(delta_I) > threshold:
                    if fist_ed == 1:
                        std = DTW(C_2s[(loop_I_cycle - 30) * 32:(loop_I_cycle + 30) * 32])
                        print('std: ',std)
                        if std > 2.0:

                            plt.plot(C_2s)
                            plt.savefig('C:/Users/hao/Desktop/cnn_train/grey/c/' + 'C2s_' + str(i) + '_' + str(
                                img_num) + '.jpg')
                            plt.close()

                            print('np.abs(delta_I): ', np.abs(delta_I),'loop_I_cycle: ',loop_I_cycle,len(I_cycle))
                            if Power[i] - Power[i - 1] > 0:
                                print('i: ', i, ' Power: ', Power[i], ' ', Power[i] - Power[i - 1], '  ON')
                                Power_status = 1
                                separate_current = seperate_current_on(voltage, current, i)

                                toscale_V = voltage[(i + 1) * 1920:(i + 2) * 1920]
                                max_voltage = []  # 電壓正規化
                                for loop_max in range(0, int(len(toscale_V) / 32)):
                                    max_voltage.append(np.max(toscale_V[loop_max * 32:(loop_max + 1) * 32]))
                                scale_voltage = stander_voltage / np.mean(max_voltage)

                            else:
                                if Power[i - 1] - Power[i - 2] > P_threshold:
                                    print('i: ', i, ' Power: ', Power[i], ' ', Power[i] - Power[i - 1], '  ON !!')
                                    Power_status = 1
                                    separate_current = seperate_current_on(voltage, current, i)

                                    toscale_V = voltage[(i + 1) * 1920:(i + 2) * 1920]
                                    max_voltage = []  # 電壓正規化
                                    for loop_max in range(0, int(len(toscale_V) / 32)):
                                        max_voltage.append(np.max(toscale_V[loop_max * 32:(loop_max + 1) * 32]))
                                    scale_voltage = stander_voltage / np.mean(max_voltage)

                                else:
                                    print('i: ', i, ' Power: ', Power[i], ' ', Power[i] - Power[i - 1], '  OFF')
                                    Power_status = -1
                                    separate_current = seperate_current_off(voltage, current, i)

                                    toscale_V = voltage[(i - 2) * 1920:(i - 1) * 1920]
                                    max_voltage = []  # 電壓正規化
                                    for loop_max in range(0, int(len(toscale_V) / 32)):
                                        max_voltage.append(np.max(toscale_V[loop_max * 32:(loop_max + 1) * 32]))
                                    scale_voltage = stander_voltage / np.mean(max_voltage)

                            plt.plot(separate_current)
                            plt.savefig('C:/Users/hao/Desktop/cnn_train/grey/c/' + 'separate_current_' + str(i)+'_' + str(
                                img_num) + '.jpg')
                            plt.close()

                            max_index = mix_load(separate_current, scale_voltage, img_num,Power_status)
                            print('img_num: ', img_num)
                            print('max_index: ',max_index,' fist_ed: ',fist_ed)
                            img_num = img_num + 1
                            fist_ed = 2
                        else:
                            fist_ed = 2


            check_ED.append(1)

            # print('separate_current: ', len(separate_current), ' img_num: ', img_num)
            # max_index, fist_ed = mix_load(separate_current, scale_voltage, img_num,Power_status)
            # print('max_index: ',max_index,' fist_ed: ',fist_ed)
            # img_num = img_num + 1

        else:
            thd_list.append(0)


        if (i + 1) * 1920 >= len(current) - 1920*5:
            plt.subplot(7, 1, 1)
            plt.ylabel('current(A)')
            plt.plot(current)
            plt.subplot(7, 1, 2)
            plt.ylabel('electric pot')
            plt.plot(load_S)
            plt.subplot(7, 1, 3)
            plt.ylabel('oven')
            plt.plot(load_O)
            plt.subplot(7, 1, 4)
            plt.xlabel('time(S)')
            plt.ylabel('microwave oven')
            plt.plot(load_M)
            plt.subplot(7, 1, 5)
            plt.xlabel('time(S)')
            plt.ylabel('hair dryer')
            plt.plot(load_H)
            plt.subplot(7, 1, 6)
            plt.xlabel('time(S)')
            plt.ylabel('Induction cooker')
            plt.plot(load_I)
            plt.subplot(7, 1, 7)
            plt.xlabel('time(S)')
            plt.ylabel('pc')
            plt.plot(load_pc)
            plt.show()

            break
        else:
            if fist_ed > 1:
                # max_index = max(status, key=status.count)
                # print('status: ',status,'  max_index',max_index)
                print('max_index: ',max_index,' fist_ed: ',fist_ed)
                print('////////////////////////////////////////////////////////////')
                if max_index == 0:
                    if Power[i] - Power[i-1] > 0:
                        status_S = 1
                    else:
                        status_S = 0
                elif max_index == 1:

                    if Power[i] - Power[i-1] > 0:
                        status_O = 1
                    else:
                        status_O = 0
                elif max_index == 4:      # 微波爐 4
                    # load_M.append(status_on)
                    if Power[i] - Power[i-1] > 0:
                        status_M = 1
                    else:
                        status_M = 0
                elif max_index == 2:     # 吹風機 2
                    if Power[i] - Power[i-1] > 0:
                        status_H = 1
                    else:
                        status_H = 0
                elif max_index == 3 :   # 電磁爐 3
                    if Power[i] - Power[i-1] > 0:
                        status_I = 1
                    else:
                        status_I = 0
                elif max_index == 5 :   # 電腦 5
                    if Power_status == 1:
                        status_pc = 1
                    elif Power_status == -1:
                        status_pc = 0
            load_S.append(status_S)
            load_O.append(status_O)
            load_M.append(status_M)
            load_H.append(status_H)
            load_I.append(status_I)
            load_pc.append(status_pc)
            i = i + 1
            fist_ed = 1

def mix_load(separate_current, scale_voltage, img_num, Power_status):

    thd_threshold=6 #20->15
    threshold=0.5

    THD=0
    img_size = 32;

    I_cycle = []
    for loop_cycle in range(0, int(len(separate_current) / 32)):
        val_to_mean = separate_current[loop_cycle * 32:(loop_cycle + 1) * 32]
        I_cycle_mean = np.mean(val_to_mean)
        signal_to_filter = [x - I_cycle_mean for x in
                            separate_current[loop_cycle * 32:(loop_cycle + 1) * 32]]
        I_cycle.append(np.sum(np.abs(signal_to_filter)) / 32)

    to_thd = []
    loop_thd = 0
    fist_ed = 1
    for loop_I_cycle in range(0, len(I_cycle) - 1):
        delta_I = I_cycle[loop_I_cycle + 1] - I_cycle[loop_I_cycle]
        if np.abs(delta_I) > threshold:
            if fist_ed == 1:
                for loop_to_thd in range(0, 1920):
                    if loop_to_thd < 960:
                        to_thd.append(0)
                    else:
                        to_thd.append(separate_current[(loop_I_cycle * 32 + 64) + loop_thd])
                        loop_thd = loop_thd + 1
                fist_ed=2
                break

    print('len(separate_current)',len(separate_current))

    plt.plot(to_thd)
    plt.savefig('C:/Users/hao/Desktop/cnn_train/grey/c/' + 'mix_thd' + str(img_num) + '.jpg')
    plt.close()
    THD = thd(to_thd)
    print("THD : ", THD)

    if THD < thd_threshold:  # 線性
        img_C = to_thd[1024:1024 + img_size * 10]
        new_img = []
        for loop_img_C in range(1, len(img_C)):
            if img_C[loop_img_C] > 0 and img_C[loop_img_C - 1] < 0:
                for loop_scale in range(0, len(img_C)):
                    if loop_scale < len(img_C) - loop_img_C:
                        new_img.append(img_C[loop_scale + loop_img_C])
                    else:
                        new_img.append(0)
                break
        print('new_img: ', len(new_img))
        new_img = [x * scale_voltage for x in new_img]
        plt.plot(new_img)
        plt.ylim((-20, 20))
        save_img_addr = 'C:/Users/hao/Desktop/cnn_train/grey/mix/'
        plt.savefig(save_img_addr + str(img_num) + '.jpg')
        plt.close()

        save_resizeimg_addr = 'C:/Users/hao/Desktop/cnn_train/grey/64/mix/'
        img = Image.open(save_img_addr + str(img_num) + '.jpg')
        img = img.resize((64, 64))
        L = img.convert('L')
        L.save(save_resizeimg_addr + str(img_num) + '.jpg')

        max_index = evaluate_one_image(save_resizeimg_addr + str(img_num) + '.jpg',
                                       'C:/Users/hao/Desktop/cnn_train/model_weight/OW_1920_SOHI', 4)
    else:  # 非線性

        # std = DTW(separate_current[(loop_I_cycle - 30) * 32:(loop_I_cycle + 30) * 32])
        # if Power_status == 1:
        img_C = separate_current[(loop_I_cycle + 2) * 32:(loop_I_cycle + 2) * 32 + img_size ** 2]
        # elif Power_status == -1:
        #     img_C = separate_current[(loop_I_cycle - 2) * 32 - img_size ** 2:(loop_I_cycle - 2) * 32]

        plt.plot(img_C)
        plt.savefig('C:/Users/hao/Desktop/cnn_train/grey/c/' + str(img_num) + '.jpg')
        plt.close()
        new_img = []
        start_zero = 1
        while 1:
            if img_C[start_zero] > 0 and img_C[start_zero - 1] < 0:
                for loop_scale in range(0, len(img_C)):
                    if loop_scale < len(img_C) - start_zero:
                        new_img.append(img_C[loop_scale + start_zero])
                    else:
                        new_img.append(0)
                img_2D = [[0 for _ in range(img_size)] for _ in range(img_size)]
                for row in range(0, img_size):
                    for col in range(0, img_size):
                        img_2D[row][col] = new_img[row * img_size + col]
                plt.imshow(img_2D)
                plt.xticks([])
                plt.yticks([])
                save_img_addr = 'C:/Users/hao/Desktop/cnn_train/grey/mix/'
                plt.savefig(save_img_addr + str(img_num) + '.jpg')
                plt.close()

                save_resizeimg_addr = 'C:/Users/hao/Desktop/cnn_train/grey/64/mix/'
                img = Image.open(save_img_addr + str(img_num) + '.jpg')
                img = img.resize((64, 64))
                L = img.convert('L')
                L.save(save_resizeimg_addr + str(img_num) + '.jpg')

                max_index = evaluate_one_image(save_resizeimg_addr + str(img_num) + '.jpg',
                                               'C:/Users/hao/Desktop/cnn_train/model_weight/2Dgrey_1920_Mpc',
                                               2)
                max_index = max_index + 4
                break
            else:
                start_zero = start_zero + 1




    return max_index

def seperate_current_on(voltage,current,i):
    V_i_0, C_i_0 = FIT_PS.fit_ps(voltage[(i + 1) * 1920:(i + 2) * 1920], current[(i + 1) * 1920:(i + 2) * 1920])  # +1
    V_i, C_i = FIT_PS.fit_ps(voltage[i * 1920:(i + 1) * 1920], current[i * 1920:(i + 1) * 1920])
    V_i_1, C_i_1 = FIT_PS.fit_ps(voltage[(i - 1) * 1920:i * 1920], current[(i - 1) * 1920:i * 1920])  # -1

    V_i_2, C_i_2 = FIT_PS.fit_ps(voltage[(i - 3) * 1920:(i - 2) * 1920], current[(i - 3) * 1920:(i - 2) * 1920])

    arr_C_i_0 = np.array(C_i_0)  # +1
    arr_C_i = np.array(C_i)
    arr_C_i_1 = np.array(C_i_1)  # -1
    arr_C_i_2 = np.array(C_i_2)

    temp_arr_0 = arr_C_i_0 - arr_C_i_2
    temp_arr_1 = arr_C_i - arr_C_i_2
    temp_arr_2 = arr_C_i_1 - arr_C_i_2

    list_0 = []
    for loop_temp_arr in range(0, len(temp_arr_0)):
        list_0.append(temp_arr_0[loop_temp_arr])
    list_1 = []
    for loop_temp_arr in range(0, len(temp_arr_1)):
        list_1.append(temp_arr_1[loop_temp_arr])
    list_2 = []
    for loop_temp_arr in range(0, len(temp_arr_2)):
        list_2.append(temp_arr_2[loop_temp_arr])

    separate_current = list_2 + list_1 + list_0
    return separate_current

def seperate_current_off(voltage,current,i):
    V_i_0, C_i_0 = FIT_PS.fit_ps(voltage[(i + 1) * 1920:(i + 2) * 1920], current[(i + 1) * 1920:(i + 2) * 1920])
    V_i, C_i = FIT_PS.fit_ps(voltage[i * 1920:(i + 1) * 1920], current[i * 1920:(i + 1) * 1920])
    V_i_1, C_i_1 = FIT_PS.fit_ps(voltage[(i - 1) * 1920:i * 1920], current[(i - 1) * 1920:i * 1920])

    V_i_2, C_i_2 = FIT_PS.fit_ps(voltage[(i - 3) * 1920:(i - 2) * 1920], current[(i - 3) * 1920:(i - 2) * 1920])

    arr_C_i_0 = np.array(C_i_0)  # +1
    arr_C_i = np.array(C_i)
    arr_C_i_1 = np.array(C_i_1)  # -1
    arr_C_i_2 = np.array(C_i_2)

    temp_arr_0 = arr_C_i_2 - arr_C_i_0
    temp_arr_1 = arr_C_i_2 - arr_C_i
    temp_arr_2 = arr_C_i_2 - arr_C_i_1

    list_0 = []
    for loop_temp_arr in range(0, len(temp_arr_0)):
        list_0.append(temp_arr_0[loop_temp_arr])
    list_1 = []
    for loop_temp_arr in range(0, len(temp_arr_1)):
        list_1.append(temp_arr_1[loop_temp_arr])
    list_2 = []
    for loop_temp_arr in range(0, len(temp_arr_2)):
        list_2.append(temp_arr_2[loop_temp_arr])

    separate_current = list_2 + list_1 + list_0
    return separate_current


# def DTW(voltage,current):
#     DTW_distance=[]
#     zero_points=[]
#     for loop_cycle in range(0,len(current)-1):  # 電壓週期 1秒
#         if voltage[loop_cycle + 1] > 0 and voltage[loop_cycle] < 0:
#             zero_points.append(loop_cycle)
#
#     for loop_zero_points in range(0, len(zero_points)-2):
#         I_cycle1=current[zero_points[loop_zero_points]:zero_points[loop_zero_points + 1]]  ## 第一個週期
#         I_cycle2=current[zero_points[loop_zero_points+1]:zero_points[loop_zero_points + 2]]  ## 第二個週期
#         distance, path = fastdtw(I_cycle1, I_cycle2, dist=euclidean)
#         DTW_distance.append(distance)
#
#     std_DTW=np.std(DTW_distance, ddof=1)
#     # for loop_DTW_distance in range(0,len(DTW_distance)):
#
#     return std_DTW
def DTW(current):
    DTW_distance=[]
    zero_points=[]

    for loop_max in range(0, int(len(current)/32)-1):
        I_cycle1=current[loop_max * 32:(loop_max + 1) * 32]  ## 第一個週期
        I_cycle2=current[(loop_max + 1) * 32:(loop_max + 2) * 32]  ## 第二個週期
        distance, path = fastdtw(I_cycle1, I_cycle2, dist=euclidean)
        DTW_distance.append(distance)

    std_DTW=np.std(DTW_distance, ddof=1)

    return std_DTW

def evaluate_one_image(img_dir,logs_train_dir,N_CLASSES):

    # logs_train_dir='C:/Users/hao/Desktop/cnn_train/model_weight/ZV_DTW_SO'

    for img_name in range(0,1):
        img = Image.open(img_dir)
        imag = img.resize([64, 64])  # 由于图片在预处理阶段以及resize，因此该命令可略
        image_array = np.array(imag)
        # image_array = image
        with tf.Graph().as_default():
            BATCH_SIZE = 1
            # N_CLASSES = 3

            image = tf.cast(image_array, tf.float32)
            # image = tf.image.per_image_standardization(image)
            image = tf.reshape(image, [1, 64, 64, 1])
            # image = tf.reshape(image, [1, 64, 64, 3])

            logit = CNN_model.inference(image, BATCH_SIZE, N_CLASSES)
            logit = tf.nn.softmax(logit)
            x = tf.placeholder(tf.float32, shape=[64, 64])
            # x = tf.placeholder(tf.float32, shape=[64, 64, 3])
            saver = tf.train.Saver()

            with tf.Session() as sess:
                ckpt = tf.train.get_checkpoint_state(logs_train_dir)
                if ckpt and ckpt.model_checkpoint_path:
                    saver.restore(sess, ckpt.model_checkpoint_path)

                prediction = sess.run(logit, feed_dict={x: image_array})
                max_index=np.argmax(prediction)
    return  max_index


def thd(sampled_data):
    abs_yf = np.abs(fft(sampled_data))
    abs_data=abs_yf[1:int(len(abs_yf) / 2)]
    # sq_sum=0.0
    # y = sorted(abs_data, reverse=True)
    sq_sum=abs_data[179]**2+abs_data[299]**2+abs_data[419]**2
    # sq_sum = y[1] ** 2 + y[2] ** 2 + y[3] ** 2
    # for r in range( len(abs_data)):
    #    sq_sum = sq_sum + (abs_data[r])**2
    # sq_harmonics = sq_sum -abs_data[59]**2.0
    sq_harmonics=sq_sum**0.5
    thd = 100*sq_harmonics / abs_data[59]

    return thd



if __name__ == '__main__':
    run()