
## 製作事件檢測訓練數據

import numpy as np
import matplotlib.pyplot as plt

filename = 'C:/Users/hao/Desktop/cnn_train/grey/temp_zone/train_ED/para1.txt'  # 電壓數據位置
f = open(filename, 'r')
line = f.read()
list = line.split('\n')
one_derivatives = np.array(list, dtype=float)

save_EDimg_addr='C:/Users/hao/Desktop/cnn_train/grey/temp_zone/train_ED/ED_database/'

ED_imgnum=0

rerange=[]

li=one_derivatives.tolist()
a=len(one_derivatives)
print('a',a,len(li))

for i in range(0,a):

    plt.plot(li[i:len(li)])
    plt.savefig(save_EDimg_addr + str(ED_imgnum)  + '.jpg')
    plt.close()




    b=li[i:len(li)]
    print(len(b))
    derivative_2D = [[0 for _ in range(len(b))] for _ in range(len(b))]

    for loop_row in range(0, len(b)):
        for loop_column in range(0, len(b)):
            derivative_2D[loop_row][loop_column] = b[loop_column]

    plt.imshow(derivative_2D)
    plt.xticks([])
    plt.yticks([])
    plt.savefig('C:/Users/hao/Desktop/cnn_train/grey/temp_zone/train_ED/ED_database/2D/' + str(ED_imgnum)  + '.jpg')
    plt.close()

    # print(li)
    li.append(li[i])
    ED_imgnum=ED_imgnum + 1
    if np.max(li[i:len(li)]) < 0.2:
        break

# for i in range(0,len(one_derivatives)):
#
#     plt.plot(one_derivatives[i:len(one_derivatives)])
#     plt.savefig(save_EDimg_addr + str(ED_imgnum)  + '.jpg')
#     plt.close()
#
#     np.insert(one_derivatives,0,one_derivatives[len(one_derivatives)-1])
#     ED_imgnum=ED_imgnum + 1
#     if np.max(one_derivatives[i:len(one_derivatives)]) < 0.2:
#         break