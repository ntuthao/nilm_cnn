import numpy as np

def RMS_transform_period(voltage,current):

    zero=[]
    RMS_Currenr_period=[]

    for i in range(0,len(voltage)-1):
        if voltage[i+1]>0 and voltage[i]<0:
            zero.append(i)
        elif voltage[i+1]>0 and voltage[i]==0 and voltage[i-1]<0:
            zero.append(i)

    for loop_zero in range(0,len(zero)-1):
        Csquare = np.power(current[zero[loop_zero]:zero[loop_zero+1]], 2)
        sum_Csquare = np.sum(Csquare) / (len(current[zero[loop_zero]:zero[loop_zero+1]]))
        RMS_Currenr_period.append(np.power(sum_Csquare, 0.5))

    return RMS_Currenr_period