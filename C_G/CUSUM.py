import numpy as np

def CUSUM(current):

    window_size=11
    S_diff=[]

    for i in range(0,len(current)-window_size+1):
        S = []
        mean_rms=np.mean(current[i:i+window_size-1])
        S_temp = 0;
        for j in range(i,i+window_size-1):
            S_temp=S_temp+(current[j]-mean_rms)
            S.append(S_temp)

        S_diff.append((np.max(S)-np.min(S)))


    return S_diff
