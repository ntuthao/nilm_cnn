
import numpy as np
import matplotlib.pyplot as plt
import os
from PIL import Image
from scipy.spatial.distance import euclidean
from fastdtw import fastdtw
import tensorflow as tf
from scipy.fftpack import fft ,ifft

import RMS_transform as rms
import RMS_transform_period as rms_period
import del_file as del_file
import CNN_model
import FIT_PS
import CUSUM

def ED_mean(meanED_voltage,meanED_current):

    # meanED_voltage = voltage[i * 1920:(i + 1) * 1920]
    # meanED_current = current[i * 1920:(i + 1) * 1920]

    mean_threshold = 0.7
    zero = []
    for loop_zero in range(0, len(meanED_voltage) - 1):
        if meanED_voltage[loop_zero + 1] > 0 and meanED_voltage[loop_zero] < 0:
            zero.append(loop_zero)
        elif meanED_voltage[loop_zero + 1] > 0 and meanED_voltage[loop_zero] == 0 and meanED_voltage[loop_zero - 1] < 0:
            zero.append(loop_zero)

    I_cycle = []
    for loop_cycle in range(0, len(zero) - 1):
        val_to_mean = meanED_current[zero[loop_cycle]:zero[loop_cycle + 1]]
        I_cycle_mean = np.mean(val_to_mean)
        signal_to_filter = [x - I_cycle_mean for x in
                            meanED_current[zero[loop_cycle]:zero[loop_cycle + 1]]]
        I_cycle.append(np.sum(np.abs(signal_to_filter)) / len(meanED_current[zero[loop_cycle]:zero[loop_cycle + 1]]))

    fist_meanED = 1
    for loop_I_cycle in range(0, len(I_cycle) - 1):
        delta_I = I_cycle[loop_I_cycle + 1] - I_cycle[loop_I_cycle]
        if np.abs(delta_I) > mean_threshold and fist_meanED == 1:
            true_EDmean_position=loop_I_cycle
            fist_meanED = 2
            break

    return true_EDmean_position, fist_meanED


def ED_CNN(current_rms_period,save_EDimg_addr,save_EDimg64_addr,CNN_ED_imgnum):

    one_derivative = []
    for loop_S_diff in range(1, len(current_rms_period)):  # len(current_rms_period) = 59
        one_derivative.append(current_rms_period[loop_S_diff] - current_rms_period[loop_S_diff - 1])

    derivative_2D = [[0 for _ in range(len(one_derivative))] for _ in range(len(one_derivative))]

    for loop_row in range(0, len(one_derivative)):
        for loop_column in range(0, len(one_derivative)):
            derivative_2D[loop_row][loop_column] = one_derivative[loop_column]

    plt.imshow(derivative_2D)
    plt.xticks([])
    plt.yticks([])
    plt.savefig(save_EDimg_addr + str(CNN_ED_imgnum) + '_' + str(i) + '.jpg')
    plt.close()

    img = Image.open(save_EDimg_addr + str(CNN_ED_imgnum) + '_' + str(i) + '.jpg')
    img = img.resize((64, 64))
    L = img.convert('L')
    L.save(save_EDimg64_addr + str(CNN_ED_imgnum) + '.jpg')

    max_index = evaluate_one_image(save_EDimg64_addr + str(CNN_ED_imgnum) + '.jpg',
                                   'C:/Users/hao/Desktop/cnn_train/model_weight/ED_2D_makedata', 2)

    return max_index