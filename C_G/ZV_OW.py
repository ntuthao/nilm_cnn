

# 特徵：原始波形  改事件檢測(mean) 電壓過零點抓一個電流週期

import numpy as np
import matplotlib.pyplot as plt
import os
from PIL import Image
from scipy.spatial.distance import euclidean
from fastdtw import fastdtw

import RMS_transform as rms
import del_file as del_file

def run():

    filename_V = 'C:/Users/hao/Desktop/數據/I_0417_1100w_CT_V.txt'  # 電壓數據位置
    filename_C = 'C:/Users/hao/Desktop/數據/I_0417_1100w_CT_C.txt'  # 電流數據位置

    # I_500w_CT_C   S2_0412_CT_C  H_0409_CT_C    I_0415_700w_CT_V

    # filename_V = 'C:/Users/hao/Desktop/數據/demo_data/mixdata_MO_V.txt'  # 電壓數據位置
    # filename_C = 'C:/Users/hao/Desktop/數據/demo_data/mixdata_MO_C.txt'  # 電流數據位置

    f = open(filename_V, 'r')
    line = f.read()
    list = line.split('\n')
    voltage = np.array(list, dtype=float)

    f = open(filename_C, 'r')
    line = f.read()
    list = line.split('\n')
    current = np.array(list, dtype=float)

    i = 0;

    P_threshold = 100;
    threshold = 1;
    std_threshold=2;
    img_size = 33;
    figure_num = 0;

    save_img_addr = 'C:/Users/hao/Desktop/cnn_train/grey/OW/I_1100/'
    save_resizeimg_addr = 'C:/Users/hao/Desktop/cnn_train/grey/64/OW/I_1100/'
    del_file.del_file(save_img_addr)
    del_file.del_file(save_resizeimg_addr)
    del_file.del_file('C:/Users/hao/Desktop/cnn_train/grey/c/')

    Power = rms.RMS_transform(voltage, current)

    zero_points = []
    I_cycle = []
    I_cycle_start = []
    # std_DTW_arr=[]

    check_ed=0
    # check_ed=[0]*len(Power)
    std_DTW_arr = [0]*len(Power)


    while 1:
        if np.abs(Power[i + 1] - Power[i]) > P_threshold:

            ## (i-1)*2000,(i+2)*2000-1  三秒內電壓週期
            ## i*2000,(i+1)*2000-1+1000 事件發生一秒加半秒
            if Power[i + 1] - Power[i] > 0:
                for loop_cycle in range(i * 2000, (i + 2) * 2000):  # 電壓週期 2秒
                    if voltage[loop_cycle + 1] > 0 and voltage[loop_cycle] < 0:
                        zero_points.append(loop_cycle)
            else:
                for loop_cycle in range(i * 2000, (i + 2) * 2000):  # 電壓週期 2秒
                    if voltage[loop_cycle + 1] > 0 and voltage[loop_cycle] < 0:
                        zero_points.append(loop_cycle)

            for loop_zero_points in range(0, len(zero_points) - 1):
                val_to_mean = current[zero_points[loop_zero_points]:zero_points[loop_zero_points + 1]]  # ptyhon會自動扣一
                I_cycle_mean = np.mean(val_to_mean)
                signal_to_filter = [x - I_cycle_mean for x in
                                    current[zero_points[loop_zero_points]:zero_points[loop_zero_points + 1]]]
                I_cycle.append(np.sum(np.abs(signal_to_filter)) /
                               (zero_points[loop_zero_points + 1] - 1 - zero_points[loop_zero_points] + 1))
                I_cycle_start.append(zero_points[loop_zero_points])

            fist_ed = 1
            for loop_I_cycle in range(0, len(I_cycle) - 1):
                delta_I = I_cycle[loop_I_cycle + 1] - I_cycle[loop_I_cycle]
                if np.abs(delta_I) > threshold:
                    if fist_ed==1:
                        check_ed = check_ed+1
                        if Power[i + 1] - Power[i] > 0:
                            img_C = current[zero_points[loop_I_cycle+2]:zero_points[loop_I_cycle + 2] + img_size *10]

                        else:
                            img_C = current[zero_points[loop_I_cycle -1] - img_size *10:zero_points[loop_I_cycle -1]]


                        new_img = []
                        start_zero = 1

                        while 1:
                            if img_C[start_zero] > 0 and img_C[start_zero - 1] < 0:
                                for loop_scale in range(0, len(img_C)):
                                    if loop_scale < len(img_C) - start_zero:
                                        new_img.append(img_C[loop_scale + start_zero])
                                    else:
                                        new_img.append(0)

                                print('figure_num: ', figure_num, ' i: ', i, ' ', Power[i + 1] - Power[i])
                                # plt.imshow(img_2D, cmap="gray")
                                plt.plot(new_img)
                                plt.ylim((-20, 20))
                                # plt.xticks([])
                                # plt.yticks([])
                                plt.savefig(save_img_addr + str(figure_num) + '.jpg')
                                plt.close()
                                # if figure_num>59 and figure_num<62:
                                #     print('i ',i,' ',Power[i + 1] - Power[i])
                                plt.plot(img_C)
                                plt.savefig('C:/Users/hao/Desktop/cnn_train/grey/c/' + str(figure_num) + '.png')
                                plt.close()
                                figure_num = figure_num + 1
                                # plt.close()
                                break
                            else:
                                start_zero = start_zero + 1
                        fist_ed = fist_ed + 1

        else:
            std_DTW_arr[i]=0



        if (i + 1) * 2000 >= len(current) - 4000:
            # plt.subplot(4, 1, 1)
            # plt.plot(current)
            # plt.subplot(4, 1, 2)
            # plt.plot(Power)
            # plt.subplot(4, 1, 3)
            # plt.plot(check_ed)
            #
            # plt.show()
            # plt.close()
            print('finish')
            print(check_ed)
            break
        else:
            i = i + 1
            zero_points = []
            I_cycle = []
            I_cycle_start = []
            img = []



    for img_name in os.listdir(save_img_addr):
        img_path = save_img_addr + img_name
        img = Image.open(img_path)
        img = img.resize((64, 64))
        L = img.convert('L')
        L.save(save_resizeimg_addr + str(img_name) + '.jpg')

def DTW(voltage,current):
    DTW_distance=[]
    zero_points=[]
    for loop_cycle in range(0,len(current)-1):  # 電壓週期 1秒
        if voltage[loop_cycle + 1] > 0 and voltage[loop_cycle] < 0:
            zero_points.append(loop_cycle)

    for loop_zero_points in range(0, len(zero_points)-2):
        I_cycle1=current[zero_points[loop_zero_points]:zero_points[loop_zero_points + 1]]  ## 第一個週期
        I_cycle2=current[zero_points[loop_zero_points+1]:zero_points[loop_zero_points + 2]]  ## 第二個週期
        distance, path = fastdtw(I_cycle1, I_cycle2, dist=euclidean)
        DTW_distance.append(distance)

    std_DTW=np.std(DTW_distance, ddof=1)
    # for loop_DTW_distance in range(0,len(DTW_distance)):

    return std_DTW



if __name__ == '__main__':
    run()