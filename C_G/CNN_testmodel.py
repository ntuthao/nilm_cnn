
from PIL import Image
import numpy as np
import os
import tensorflow as tf
import matplotlib.pyplot as plt
import CNN_model
from CNN_preimg import get_files

def evaluate_all_image(img_dir):
    classes = {'S', 'O', 'M'}
    results = []
    flag = 1
    for index, name in enumerate(classes):
        class_path = img_dir + name + "/"
        for img_name in os.listdir(class_path):
            img_path = class_path + img_name
            img = Image.open(img_path)
            # plt.imshow(img)
            imag = img.resize([64, 64])  # 由于图片在预处理阶段以及resize，因此该命令可略
            image_array = np.array(imag)
            # image_array = image
            with tf.Graph().as_default():
                BATCH_SIZE = 1
                N_CLASSES = 3

                image = tf.cast(image_array, tf.float32)
                # image = tf.image.per_image_standardization(image)
                image = tf.reshape(image, [1, 64, 64, 1])

                logit = CNN_model.inference(image, BATCH_SIZE, N_CLASSES)
                logit = tf.nn.softmax(logit)
                x = tf.placeholder(tf.float32, shape=[64, 64])

                # you need to change the directories to yours.
                logs_train_dir = 'C:/Users/hao/Desktop/cnn_train/model_weight/ZV_mean'
                # logs_train_dir = 'C:/Users/hao/Desktop/cnn_train/model_weight/gray_dpi400'
                saver = tf.train.Saver()

                with tf.Session() as sess:

                    print("Reading checkpoints...")
                    ckpt = tf.train.get_checkpoint_state(logs_train_dir)
                    if ckpt and ckpt.model_checkpoint_path:
                        global_step = ckpt.model_checkpoint_path.split('/')[-1].split('-')[-1]
                        saver.restore(sess, ckpt.model_checkpoint_path)
                        # print('Loading success, global_step is %s' % global_step)

                    prediction = sess.run(logit, feed_dict={x: image_array})
                    max_index = np.argmax(prediction)
                    results.append(max_index)
                    print('max_index ', name, ' ', flag, '  :', max_index)
                    flag = flag + 1
                    if max_index == 0:
                        print('This is a S with possibility %.6f' % prediction[:, 0])
                    elif max_index == 1:
                        print('This is a O with possibility %.6f' % prediction[:, 1])
                    elif max_index == 2:
                        print('This is a M with possibility %.6f' % prediction[:, 2])

def evaluate_mix_image(img_dir):
    results = []
    flag = 0
    for img_name in os.listdir(img_dir):
        img_path = img_dir + img_name
        img = Image.open(img_path)
        # plt.imshow(img)
        imag = img.resize([64, 64])  # 由于图片在预处理阶段以及resize，因此该命令可略
        image_array = np.array(imag)
        # image_array = image
        with tf.Graph().as_default():
            BATCH_SIZE = 1
            N_CLASSES = 3

            image = tf.cast(image_array, tf.float32)
            # image = tf.image.per_image_standardization(image)
            image = tf.reshape(image, [1, 64, 64, 1])

            logit = CNN_model.inference(image, BATCH_SIZE, N_CLASSES)
            logit = tf.nn.softmax(logit)
            # print('logit ', logit)
            x = tf.placeholder(tf.float32, shape=[64, 64])

            # you need to change the directories to yours.
            logs_train_dir = 'C:/Users/hao/Desktop/cnn_train/model_weight/ZV_mean'
            # logs_train_dir = 'C:/Users/hao/Desktop/cnn_train/model_weight/gray_dpi400'
            saver = tf.train.Saver()

            with tf.Session() as sess:
                # print("Reading checkpoints...")
                ckpt = tf.train.get_checkpoint_state(logs_train_dir)
                if ckpt and ckpt.model_checkpoint_path:
                    # global_step = ckpt.model_checkpoint_path.split('/')[-1].split('-')[-1]
                    saver.restore(sess, ckpt.model_checkpoint_path)
                    # print('Loading success, global_step is %s' % global_step)

                prediction = sess.run(logit, feed_dict={x: image_array})
                max_index = np.argmax(prediction)
                # results.append(max_index)
                print('max_index ', flag, '  :', max_index,'  ',prediction)
                flag = flag + 1
                # if max_index == 0:
                #     print('This is a S with possibility %.6f' % prediction[:, 0])
                # elif max_index == 1:
                #     print('This is a O with possibility %.6f' % prediction[:, 1])
                # elif max_index == 2:
                #     print('This is a M with possibility %.6f' % prediction[:, 2])

            # return max_index

if __name__ == '__main__':
    # train_dir = 'C:/Users/hao/Desktop/cnn_train/tf/dpi400'
    # train, train_label, val, val_label = get_files(train_dir, 0.3)
    # img = get_one_image(val)  # 通过改变参数train or val，进而验证训练集或测试集
    # evaluate_one_image(img)

    # evaluate_all_image('C:/Users/hao/Desktop/cnn_train/grey/64/')
    evaluate_mix_image('C:/Users/hao/Desktop/cnn_train/grey/64/mix/')