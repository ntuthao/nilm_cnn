

# 轉灰階論文 改事件檢測(mean) 電壓過零點抓一個電流週期  DTW判斷穩態

import numpy as np
import matplotlib.pyplot as plt
import os
from PIL import Image
from scipy.spatial.distance import euclidean
from fastdtw import fastdtw

import RMS_transform as rms
import del_file as del_file

def run():

    filename_V = 'C:/Users/hao/Desktop/數據/I_500w_CT_V.txt'  # 電壓數據位置
    filename_C = 'C:/Users/hao/Desktop/數據/I_500w_CT_C.txt'  # 電流數據位置

    # filename_V = 'C:/Users/hao/Desktop/數據/demo_data/mixdata_MO_V.txt'  # 電壓數據位置
    # filename_C = 'C:/Users/hao/Desktop/數據/demo_data/mixdata_MO_C.txt'  # 電流數據位置

    f = open(filename_V, 'r')
    line = f.read()
    list = line.split('\n')
    voltage = np.array(list, dtype=float)

    f = open(filename_C, 'r')
    line = f.read()
    list = line.split('\n')
    current = np.array(list, dtype=float)

    i = 0;

    P_threshold = 100;
    threshold = 1;
    std_threshold=2;
    img_size = 33;
    figure_num = 0;

    save_img_addr = 'C:/Users/hao/Desktop/cnn_train/grey/I/'
    save_resizeimg_addr = 'C:/Users/hao/Desktop/cnn_train/grey/64/I/'
    del_file.del_file(save_img_addr)
    del_file.del_file(save_resizeimg_addr)
    del_file.del_file('C:/Users/hao/Desktop/cnn_train/grey/c/')

    Power = rms.RMS_transform(voltage, current)

    zero_points = []
    I_cycle = []
    I_cycle_start = []
    # std_DTW_arr=[]
    debug=[]
    debug_P=[]
    delta_I_mat=[]

    check_ed=[0]*len(Power)
    check_power = [0] * len(Power)
    std_DTW_arr = [0]*len(Power)
    check_ed=0
    check_edP=0


    while 1:
        if np.abs(Power[i + 1] - Power[i]) > P_threshold:
            check_edP=check_edP+1
            ## (i-1)*2000,(i+2)*2000-1  三秒內電壓週期
            ## i*2000,(i+1)*2000-1+1000 事件發生一秒加半秒
            if Power[i + 1] - Power[i] > 0:
                # check_edP=1
                for loop_cycle in range(i * 2000, (i + 2) * 2000):  # 電壓週期 1.5秒
                    if voltage[loop_cycle + 1] > 0 and voltage[loop_cycle] < 0:
                        zero_points.append(loop_cycle)
            else:
                # check_edP=0
                for loop_cycle in range(i * 2000, (i + 2) * 2000):  # 電壓週期 -1.5~1.5秒
                    if voltage[loop_cycle + 1] > 0 and voltage[loop_cycle] < 0:
                        zero_points.append(loop_cycle)

            for loop_zero_points in range(0, len(zero_points) - 1):
                val_to_mean = current[zero_points[loop_zero_points]:zero_points[loop_zero_points + 1]]  # ptyhon會自動扣一
                I_cycle_mean = np.mean(val_to_mean)
                signal_to_filter = [x - I_cycle_mean for x in
                                    current[zero_points[loop_zero_points]:zero_points[loop_zero_points + 1]]]
                I_cycle.append(np.sum(np.abs(signal_to_filter)) /
                               (zero_points[loop_zero_points + 1] - 1 - zero_points[loop_zero_points] + 1))
                I_cycle_start.append(zero_points[loop_zero_points])

            fist_ed = 1
            for loop_I_cycle in range(0, len(I_cycle) - 1):
                delta_I = I_cycle[loop_I_cycle + 1] - I_cycle[loop_I_cycle]
                if i==78:
                    delta_I_mat.append(delta_I)

                if np.abs(delta_I) > threshold:
                    if fist_ed==1:
                        check_ed = check_ed+1
                        if Power[i + 1] - Power[i] > 0:
                            # check_ed1 = 1
                            img = current[(i + 1) * 2000:(i + 1) * 2000 + img_size ** 2]
                            img_V = voltage[(i + 1) * 2000:(i + 1) * 2000 + img_size ** 2]
                            std_DTW_arr[i]=DTW(img_V, img)
                            # std_DTW_arr.append(DTW(img_V, img))
                            steady = 0;
                            while 1:
                                std_DTW = DTW(img_V, img)
                                if std_DTW > std_threshold:
                                    steady = steady + 500
                                    img = current[(i + 1) * 2000 + steady: (i + 1) * 2000 + steady + img_size ** 2]
                                    img_V = voltage[(i + 1) * 2000 + steady: (i + 1) * 2000 + steady + img_size ** 2]
                                else:
                                    break
                        else:
                            # check_ed1 = 0
                            img = current[i * 2000 - 1000 - img_size ** 2: i * 2000 - 1000]
                            img_V = voltage[i * 2000 - 1000 - img_size ** 2: i * 2000 - 1000]
                            std_DTW_arr[i] = DTW(img_V, img)
                            # std_DTW_arr.append(DTW(img_V, img))
                            steady = 0;
                            while 1:
                                std_DTW = DTW(img_V, img)
                                if std_DTW > std_threshold:
                                    steady = steady + 500
                                    img = current[
                                          (i) * 2000 - 1000 - img_size ** 2 - steady: (i) * 2000 - 1000 - steady]
                                    img_V = voltage[
                                            (i) * 2000 - 1000 - img_size ** 2 - steady: (i) * 2000 - 1000 - steady]
                                else:
                                    break

                        new_img = []
                        start_zero = 1

                        while 1:
                            if img[start_zero] > 0 and img[start_zero - 1] < 0:
                                # for loop_scale in range(0, len(img)):
                                #     if loop_scale < len(img) - start_zero:
                                #         new_img.append(img[loop_scale + start_zero])
                                #     else:
                                #         new_img.append(0)
                                # img_2D = [[0 for _ in range(img_size)] for _ in range(img_size)]
                                # # img_2D=np.zeros((img_size,img_size))
                                # for row in range(0, img_size):
                                #     for col in range(0, img_size):
                                #         img_2D[row][col] = new_img[row * img_size + col]
                                # print('figure_num: ', figure_num, ' i: ', i, ' ', Power[i + 1] - Power[i])
                                # # plt.imshow(img_2D, cmap="gray")
                                # plt.imshow(img_2D)
                                # plt.xticks([])
                                # plt.yticks([])
                                # plt.savefig(save_img_addr + str(figure_num) + '.jpg')
                                # plt.close()
                                # # if figure_num>59 and figure_num<62:
                                # #     print('i ',i,' ',Power[i + 1] - Power[i])
                                # plt.plot(new_img)
                                # plt.savefig('C:/Users/hao/Desktop/cnn_train/grey/c/' + str(figure_num) + '.jpg')
                                # plt.close()
                                # figure_num = figure_num + 1
                                # plt.close()
                                break
                            else:
                                start_zero = start_zero + 1
                        fist_ed = fist_ed + 1

            if fist_ed==1:
                debug.append([i,Power[i + 1] - Power[i],Power[i+1],Power[i]])
                # debug_P.append(Power[i + 1] - Power[i])

        else:
            std_DTW_arr[i]=0
            # check_edP = 0



        if (i + 1) * 2000 >= len(current) - 4000:
            # plt.subplot(4, 1, 1)
            # plt.plot(current)
            # plt.subplot(4, 1, 2)
            # plt.plot(Power)
            plt.subplot(4, 1, 3)
            plt.plot(delta_I_mat)
            # plt.subplot(4, 1, 4)
            # plt.plot(check_power)
            plt.show()
            # plt.close()
            print('finish')
            print(check_ed)
            print(check_edP)
            print(debug)
            print(debug_P)
            print(np.size(debug))
            print(np.size(debug_P))
            break
        else:
            i = i + 1
            zero_points = []
            I_cycle = []
            I_cycle_start = []
            img = []
            # check_ed[i] = check_ed1
            # check_power[i] = check_edP


    for img_name in os.listdir(save_img_addr):
        img_path = save_img_addr + img_name
        img = Image.open(img_path)
        img = img.resize((64, 64))
        L = img.convert('L')
        L.save(save_resizeimg_addr + str(img_name) + '.jpg')

def DTW(voltage,current):
    DTW_distance=[]
    zero_points=[]
    for loop_cycle in range(0,len(current)-1):  # 電壓週期 1秒
        if voltage[loop_cycle + 1] > 0 and voltage[loop_cycle] < 0:
            zero_points.append(loop_cycle)

    for loop_zero_points in range(0, len(zero_points)-2):
        I_cycle1=current[zero_points[loop_zero_points]:zero_points[loop_zero_points + 1]]  ## 第一個週期
        I_cycle2=current[zero_points[loop_zero_points+1]:zero_points[loop_zero_points + 2]]  ## 第二個週期
        distance, path = fastdtw(I_cycle1, I_cycle2, dist=euclidean)
        DTW_distance.append(distance)

    std_DTW=np.std(DTW_distance, ddof=1)
    # for loop_DTW_distance in range(0,len(DTW_distance)):

    return std_DTW



if __name__ == '__main__':
    run()