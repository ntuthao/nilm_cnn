
# 辨識混和負載(加入FIT-PS)  取樣頻率：1920  特徵：原始波形 、灰階圖像   事件檢測：mean + 補償機制 CNN    歐式距離分類縣性與非線性

#### 無再次檢測電流爬升位置

import numpy as np
import matplotlib.pyplot as plt
import os
from PIL import Image
from scipy.spatial.distance import euclidean
from fastdtw import fastdtw
import tensorflow as tf
from scipy.fftpack import fft ,ifft

import RMS_transform as rms
import RMS_transform_period as rms_period
import del_file as del_file
import CNN_model
import FIT_PS
import CUSUM

from sklearn.preprocessing import MinMaxScaler

def run():

    filename_V = 'C:/Users/hao/Desktop/論文數據/pc/pc2_1111_CT_1920_V.txt'  # 電壓數據位置
    filename_C = 'C:/Users/hao/Desktop/論文數據/pc/pc2_1111_CT_1920_C.txt'  # 電流數據位置

    # filename_V = 'C:/Users/hao/Desktop/數據/S_0420_CT_1920_V.txt'  # 電壓數據位置
    # filename_C = 'C:/Users/hao/Desktop/數據/S_0420_CT_1920_C.txt'  # 電流數據位置

    # filename_V = 'C:/Users/hao/Desktop/數據/I_0513_700w_CT_1920_V.txt'  # 電壓數據位置
    # filename_C = 'C:/Users/hao/Desktop/數據/I_0513_700w_CT_1920_C.txt'  # 電流數據位置

    # I_0415_500w   O_0412  H_0409_CT   0412  I_0417_  為訓練數據  0415 0416為測試數據

    # filename_V = 'C:/Users/hao/Desktop/數據/mix2_0527_CT_1920_V.txt'  # 電壓數據位置
    # filename_C = 'C:/Users/hao/Desktop/數據/mix2_0527_CT_1920_C.txt'  # 電流數據位置

    # 論文數據  S_1012_CT_1920_V
    #          I_1013_CT_1920_V


    # S_0522_CT_1920_C I_0513_700w_CT_1920_V

    # S_1012_CT_1920_V

    datebase_mean = 'C:/Users/hao/Desktop/cnn_train/grey/DB_mean.txt'  ## DB_mean_standard180 DB_mean

    f = open(filename_V, 'r')
    line = f.read()
    list = line.split('\n')
    voltage = np.array(list, dtype=float)

    f = open(filename_C, 'r')
    line = f.read()
    list = line.split('\n')
    current = np.array(list, dtype=float)

    f = open(datebase_mean, 'r')
    line = f.read()
    list = line.split('\n')
    DB_mean = np.array(list, dtype=float)

    i = 960;
    cnn_ED = 0;

    current_rms_period_threshold = 0.1
    mean_threshold = 0.7

    # save_img_addr = 'C:/Users/hao/Desktop/cnn_train/grey/temp_zone/identify_feature/'
    # save_resizeimg_addr = 'C:/Users/hao/Desktop/cnn_train/grey/temp_zone/identify_feature_64/'
    save_EDimg_addr = 'C:/Users/hao/Desktop/cnn_train/grey/temp_zone/CNN_ED/'
    save_EDimg64_addr = 'C:/Users/hao/Desktop/cnn_train/grey/temp_zone/CNN_ED64/'

    # del_file.del_file(save_img_addr)
    # del_file.del_file(save_resizeimg_addr)
    del_file.del_file(save_EDimg_addr)
    del_file.del_file(save_EDimg64_addr)
    # del_file.del_file('C:/Users/hao/Desktop/cnn_train/grey/temp_zone/current_waveform/')

    Power = rms.RMS_transform(voltage, current)

    stander_voltage_arr = []  # 標準電壓
    for loop_max in range(0, int( 5 *1920 / 32)):
        stander_voltage_arr.append(np.max(voltage[loop_max * 32:(loop_max + 1) * 32]))
    stander_voltage =np.mean(stander_voltage_arr)
    print(stander_voltage)

    img_num =0
    CNN_ED_imgnum = 0


    check_ED =[]
    thd_list =[]

    load_S = []
    load_O = []
    load_M = []
    load_H = []
    load_I = []
    load_pc = []
    load_air = []

    status_S = 0
    status_O = 0
    status_M = 0
    status_H = 0
    status_I = 0
    status_pc = 0
    status_air = 0

    check_I_cycle = []
    for loop_temp in range(0,len(Power)):
        check_ED.append(0)


    while 1:

        true_ED=0
        check_ED[i]=0

        current_rms_period=rms_period.RMS_transform_period(voltage[i * 1920:(i + 1) * 1920], current[i * 1920:(i + 1) * 1920])  ### 電流週期 rms 值
        fist_current_rms_period = 1
        # print(len(current_rms_period))
        for loop_current_rms_period in range(1,len(current_rms_period)):
            if np.abs(current_rms_period[loop_current_rms_period] - current_rms_period[loop_current_rms_period - 1]) > current_rms_period_threshold and fist_current_rms_period == 1:

                ED_position=loop_current_rms_period

                # print('in rms: ', i)
                #/////////////////////////////////////////////////////////////////////////////////////////////////////////// 二次篩選 mean #

                meanED_voltage=voltage[i * 1920:(i + 1) * 1920]
                meanED_current=current[i * 1920:(i + 1) * 1920]

                zero=[]
                for loop_zero in range(0, len(meanED_voltage) - 1):
                    if meanED_voltage[loop_zero + 1] > 0 and meanED_voltage[loop_zero] < 0:
                        zero.append(loop_zero)
                    elif meanED_voltage[loop_zero + 1] > 0 and meanED_voltage[loop_zero] == 0 and meanED_voltage[loop_zero - 1] < 0:
                        zero.append(loop_zero)

                I_cycle = []
                for loop_cycle in range(0, len(zero)-1):
                    val_to_mean = meanED_current[zero[loop_cycle]:zero[loop_cycle+1]]
                    I_cycle_mean = np.mean(val_to_mean)
                    signal_to_filter = [x - I_cycle_mean for x in
                                        meanED_current[zero[loop_cycle]:zero[loop_cycle+1]]]
                    I_cycle.append(np.sum(np.abs(signal_to_filter)) / len(meanED_current[zero[loop_cycle]:zero[loop_cycle+1]]))

                fist_meanED = 1
                for loop_I_cycle in range(0, len(I_cycle) - 1):
                    delta_I = I_cycle[loop_I_cycle + 1] - I_cycle[loop_I_cycle]
                    if np.abs(delta_I) > mean_threshold and fist_meanED==1:

                        print('in mean: ',i)
                        true_ED=1
                        check_ED[i] = 1

                        fist_meanED = 2
                        break



                #///////////////////////////////////////////////////////////////////////////////////////////////////////////  補償機制 CNN #
                if fist_meanED == 1:

                    one_derivative = []
                    for loop_S_diff in range(1, len(current_rms_period)):  # len(current_rms_period) = 59
                        one_derivative.append(current_rms_period[loop_S_diff] - current_rms_period[loop_S_diff - 1])
                        # f = open('C:/Users/hao/Desktop/cnn_train/grey/temp_zone/train_ED/para3.txt', 'a')
                        # f.write(str(current_rms_period[loop_S_diff] - current_rms_period[loop_S_diff - 1]))
                        # f.write('\n')
                        # f.close()

                    # plt.plot(one_derivative)
                    # plt.savefig(save_EDimg_addr + str(CNN_ED_imgnum) + '_one_derivative_' + str(i) + '.jpg')
                    # plt.close()

                    derivative_2D = [[0 for _ in range(len(one_derivative))] for _ in range(len(one_derivative))]

                    for loop_row in range(0, len(one_derivative)):
                        for loop_column in range(0, len(one_derivative)):
                            derivative_2D[loop_row][loop_column] = one_derivative[loop_column]

                    plt.imshow(derivative_2D)
                    plt.xticks([])
                    plt.yticks([])
                    plt.savefig(save_EDimg_addr + str(CNN_ED_imgnum) + '_' + str(i) + '.jpg')
                    plt.close()

                    img = Image.open(save_EDimg_addr + str(CNN_ED_imgnum) + '_' + str(i) + '.jpg')
                    img = img.resize((64, 64))
                    L = img.convert('L')
                    L.save(save_EDimg64_addr + str(CNN_ED_imgnum) + '.jpg')

                    max_index = evaluate_one_image(save_EDimg64_addr + str(CNN_ED_imgnum) + '.jpg',
                                                   'C:/Users/hao/Desktop/cnn_train/model_weight/ED_2D_makedata',2)

                    # print('max_index_ED: ',max_index,' ',i)
                    if max_index==1:
                        true_ED = 1
                        check_ED[i] = 1
                        print('!!!!!!!!!!!!!!! ')
                        print('max_index_ED: ', max_index, ' ', i)
                    else:
                        cnn_ED = cnn_ED + 1


                    CNN_ED_imgnum = CNN_ED_imgnum + 1


                fist_current_rms_period = 2
                break




        if (i + 1) * 1920 >= len(current) - 1920 *5:

            print('cnn_ED: ', cnn_ED)
            plt.subplot(2, 1, 1)
            plt.plot(current)
            plt.subplot(2, 1, 2)
            plt.plot(check_ED)
            plt.show()

            # plt.subplot(8, 1, 2)
            # plt.ylabel('S')

            break

        else:
            i = i + 1




def seperate_current_on(voltage ,current ,i):
    V_i_0, C_i_0 = FIT_PS.fit_ps(voltage[(i + 1) * 1920:(i + 2) * 1920], current[(i + 1) * 1920:(i + 2) * 1920])  # +1
    V_i, C_i = FIT_PS.fit_ps(voltage[i * 1920:(i + 1) * 1920], current[i * 1920:(i + 1) * 1920])
    V_i_1, C_i_1 = FIT_PS.fit_ps(voltage[(i - 1) * 1920:i * 1920], current[(i - 1) * 1920:i * 1920])  # -1

    V_i_2, C_i_2 = FIT_PS.fit_ps(voltage[(i - 3) * 1920:(i - 2) * 1920], current[(i - 3) * 1920:(i - 2) * 1920])

    arr_C_i_0 = np.array(C_i_0)  # +1
    arr_C_i = np.array(C_i)
    arr_C_i_1 = np.array(C_i_1)  # -1
    arr_C_i_2 = np.array(C_i_2)

    temp_arr_0 = arr_C_i_0 - arr_C_i_2
    temp_arr_1 = arr_C_i - arr_C_i_2
    temp_arr_2 = arr_C_i_1 - arr_C_i_2

    list_0 = []
    for loop_temp_arr in range(0, len(temp_arr_0)):
        list_0.append(temp_arr_0[loop_temp_arr])
    list_1 = []
    for loop_temp_arr in range(0, len(temp_arr_1)):
        list_1.append(temp_arr_1[loop_temp_arr])
    list_2 = []
    for loop_temp_arr in range(0, len(temp_arr_2)):
        list_2.append(temp_arr_2[loop_temp_arr])

    separate_current = list_2 + list_1 + list_0
    return separate_current

def seperate_current_off(voltage ,current ,i):
    V_i_0, C_i_0 = FIT_PS.fit_ps(voltage[(i + 1) * 1920:(i + 2) * 1920], current[(i + 1) * 1920:(i + 2) * 1920])
    V_i, C_i = FIT_PS.fit_ps(voltage[i * 1920:(i + 1) * 1920], current[i * 1920:(i + 1) * 1920])
    V_i_1, C_i_1 = FIT_PS.fit_ps(voltage[(i - 1) * 1920:i * 1920], current[(i - 1) * 1920:i * 1920])

    V_i_2, C_i_2 = FIT_PS.fit_ps(voltage[(i - 3) * 1920:(i - 2) * 1920], current[(i - 3) * 1920:(i - 2) * 1920])

    arr_C_i_0 = np.array(C_i_0)  # +1
    arr_C_i = np.array(C_i)
    arr_C_i_1 = np.array(C_i_1)  # -1
    arr_C_i_2 = np.array(C_i_2)

    temp_arr_0 = arr_C_i_2 - arr_C_i_0
    temp_arr_1 = arr_C_i_2 - arr_C_i
    temp_arr_2 = arr_C_i_2 - arr_C_i_1

    list_0 = []
    for loop_temp_arr in range(0, len(temp_arr_0)):
        list_0.append(temp_arr_0[loop_temp_arr])
    list_1 = []
    for loop_temp_arr in range(0, len(temp_arr_1)):
        list_1.append(temp_arr_1[loop_temp_arr])
    list_2 = []
    for loop_temp_arr in range(0, len(temp_arr_2)):
        list_2.append(temp_arr_2[loop_temp_arr])

    separate_current = list_2 + list_1 + list_0
    return separate_current


# def DTW(voltage,current):
#     DTW_distance=[]
#     zero_points=[]
#     for loop_cycle in range(0,len(current)-1):  # 電壓週期 1秒
#         if voltage[loop_cycle + 1] > 0 and voltage[loop_cycle] < 0:
#             zero_points.append(loop_cycle)
#
#     for loop_zero_points in range(0, len(zero_points)-2):
#         I_cycle1=current[zero_points[loop_zero_points]:zero_points[loop_zero_points + 1]]  ## 第一個週期
#         I_cycle2=current[zero_points[loop_zero_points+1]:zero_points[loop_zero_points + 2]]  ## 第二個週期
#         distance, path = fastdtw(I_cycle1, I_cycle2, dist=euclidean)
#         DTW_distance.append(distance)
#
#     std_DTW=np.std(DTW_distance, ddof=1)
#     # for loop_DTW_distance in range(0,len(DTW_distance)):
#
#     return std_DTW
def DTW(current):
    DTW_distance =[]
    zero_points =[]

    for loop_max in range(0, int(len(current ) /32 ) -1):
        I_cycle1 =current[loop_max * 32:(loop_max + 1) * 32]  ## 第一個週期
        I_cycle2 =current[(loop_max + 1) * 32:(loop_max + 2) * 32]  ## 第二個週期
        distance, path = fastdtw(I_cycle1, I_cycle2, dist=euclidean)
        DTW_distance.append(distance)

    std_DTW =np.std(DTW_distance, ddof=1)

    return std_DTW

def evaluate_one_image(img_dir ,logs_train_dir ,N_CLASSES):

    # logs_train_dir='C:/Users/hao/Desktop/cnn_train/model_weight/ZV_DTW_SO'

    for img_name in range(0 ,1):
        img = Image.open(img_dir)
        imag = img.resize([64, 64])  # 由于图片在预处理阶段以及resize，因此该命令可略
        image_array = np.array(imag)
        # image_array = image
        with tf.Graph().as_default():
            BATCH_SIZE = 1
            # N_CLASSES = 3

            image = tf.cast(image_array, tf.float32)
            # image = tf.image.per_image_standardization(image)
            image = tf.reshape(image, [1, 64, 64, 1])
            # image = tf.reshape(image, [1, 64, 64, 3])

            logit = CNN_model.inference(image, BATCH_SIZE, N_CLASSES)
            logit = tf.nn.softmax(logit)
            x = tf.placeholder(tf.float32, shape=[64, 64])
            # x = tf.placeholder(tf.float32, shape=[64, 64, 3])
            saver = tf.train.Saver()

            with tf.Session() as sess:
                ckpt = tf.train.get_checkpoint_state(logs_train_dir)
                if ckpt and ckpt.model_checkpoint_path:
                    saver.restore(sess, ckpt.model_checkpoint_path)

                prediction = sess.run(logit, feed_dict={x: image_array})
                max_index =np.argmax(prediction)
    return  max_index


def thd(sampled_data):
    abs_yf = np.abs(fft(sampled_data))
    abs_data =abs_yf[1:int(len(abs_yf) / 2)]
    # sq_sum=0.0
    # y = sorted(abs_data, reverse=True)
    sq_sum =abs_data[179 ]** 2 +abs_data[299 ]** 2 +abs_data[419 ]**2
    # sq_sum = y[1] ** 2 + y[2] ** 2 + y[3] ** 2
    # for r in range( len(abs_data)):
    #    sq_sum = sq_sum + (abs_data[r])**2
    # sq_harmonics = sq_sum -abs_data[59]**2.0
    sq_harmonics =sq_sum**0.5
    thd = 100 *sq_harmonics / abs_data[59]

    return thd



if __name__ == '__main__':
    run()