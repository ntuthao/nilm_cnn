
#  特徵萃取  特徵：原始波形  取樣頻率：1920(CT) 加入FIT-PS固定周期  電壓過零點抓一個電流週期
#  V2為論文系統

import numpy as np
import matplotlib.pyplot as plt
import os
from PIL import Image
from scipy.spatial.distance import euclidean
from fastdtw import fastdtw
import tensorflow as tf
from scipy.fftpack import fft ,ifft

import RMS_transform as rms
import RMS_transform_period as rms_period
import del_file as del_file
import CNN_model
import FIT_PS
import CUSUM
import NILM_ED

def run():

    filename_V = 'C:/Users/hao/Desktop/數據/M_1012_CT_1920_V.txt'  # 電壓數據位置
    filename_C = 'C:/Users/hao/Desktop/數據/M_1012_CT_1920_C.txt'  # 電流數據位置

    #  訓練數據：  pc2_0528_CT_1920_C  M_0420_CT_1920_C   pcting1_CT_1920_C  pcting2_CT_1920_C
    #             I_0513_700w_CT_1920_C  air_0727_1hr_CT_1920_1_V

    f = open(filename_V, 'r')
    line = f.read()
    list = line.split('\n')
    voltage = np.array(list, dtype=float)

    f = open(filename_C, 'r')
    line = f.read()
    list = line.split('\n')
    current = np.array(list, dtype=float)

    i = 299;
    CNN_ED_imgnum = 0
    figure_num = 1

    current_rms_period_threshold = 0.1
    mean_threshold = 0.6
    img_size = 32


    save_img_addr = 'C:/Users/hao/Desktop/cnn_train/grey/paper_img/'           ##  特徵圖片處存位置
    # save_resizeimg_addr = 'C:/Users/hao/Desktop/cnn_train_forpaper/FE/2D/64/air/'  ##  特徵圖片 64 處存位置
    save_EDimg_addr = 'C:/Users/hao/Desktop/cnn_train/grey/temp_zone/CNN_ED/'
    save_EDimg64_addr = 'C:/Users/hao/Desktop/cnn_train/grey/temp_zone/CNN_ED64/'
    # del_file.del_file(save_img_addr)
    # del_file.del_file(save_resizeimg_addr)
    del_file.del_file(save_EDimg_addr)
    del_file.del_file(save_EDimg64_addr)


    Power = rms.RMS_transform(voltage, current)

    check_ED=[]
    for loop_temp in range(0,len(Power)):
        check_ED.append(0)

    stander_voltage_arr = []  # 標準電壓
    for loop_max in range(0, int(5*1920 / 32)):
        stander_voltage_arr.append(np.max(voltage[loop_max * 32:(loop_max + 1) * 32]))
    stander_voltage=np.mean(stander_voltage_arr)
    print(stander_voltage)


    while 1:

        true_ED = 0
        check_ED[i] = 0

        current_rms_period = rms_period.RMS_transform_period(voltage[i * 1920:(i + 1) * 1920],current[i * 1920:(i + 1) * 1920])  ### 電流週期 rms 值
        fist_current_rms_period = 1
        # print(len(current_rms_period))
        for loop_current_rms_period in range(1, len(current_rms_period)):
            if np.abs(current_rms_period[loop_current_rms_period] - current_rms_period[loop_current_rms_period - 1]) > current_rms_period_threshold and fist_current_rms_period == 1:

                print('in rms: ', i)
                # /////////////////////////////////////////////////////////////////////////////////////////////////////////// 二次篩選 mean #

                meanED_voltage = voltage[i * 1920:(i + 1) * 1920]
                meanED_current = current[i * 1920:(i + 1) * 1920]

                zero = []
                for loop_zero in range(0, len(meanED_voltage) - 1):
                    if meanED_voltage[loop_zero + 1] > 0 and meanED_voltage[loop_zero] < 0:
                        zero.append(loop_zero)
                    elif meanED_voltage[loop_zero + 1] > 0 and meanED_voltage[loop_zero] == 0 and meanED_voltage[
                        loop_zero - 1] < 0:
                        zero.append(loop_zero)

                I_cycle = []
                for loop_cycle in range(0, len(zero) - 1):
                    val_to_mean = meanED_current[zero[loop_cycle]:zero[loop_cycle + 1]]
                    I_cycle_mean = np.mean(val_to_mean)
                    signal_to_filter = [x - I_cycle_mean for x in
                                        meanED_current[zero[loop_cycle]:zero[loop_cycle + 1]]]
                    I_cycle.append(
                        np.sum(np.abs(signal_to_filter)) / len(meanED_current[zero[loop_cycle]:zero[loop_cycle + 1]]))

                fist_meanED = 1
                for loop_I_cycle in range(0, len(I_cycle) - 1):
                    delta_I = I_cycle[loop_I_cycle + 1] - I_cycle[loop_I_cycle]
                    if np.abs(delta_I) > mean_threshold and fist_meanED == 1:
                        print('in mean: ', i)
                        true_ED = 1
                        check_ED[i] = 1

                        fist_meanED = 2
                        break

                # ///////////////////////////////////////////////////////////////////////////////////////////////////////////  補償機制 CNN #
                if fist_meanED == 1:

                    one_derivative = []
                    for loop_S_diff in range(1, len(current_rms_period)):  # len(current_rms_period) = 59
                        one_derivative.append(current_rms_period[loop_S_diff] - current_rms_period[loop_S_diff - 1])
                        # f = open('C:/Users/hao/Desktop/cnn_train/grey/temp_zone/train_ED/para3.txt', 'a')
                        # f.write(str(current_rms_period[loop_S_diff] - current_rms_period[loop_S_diff - 1]))
                        # f.write('\n')
                        # f.close()

                    # plt.plot(one_derivative)
                    # plt.savefig(save_EDimg_addr + str(CNN_ED_imgnum) + '_one_derivative_' + str(i) + '.jpg')
                    # plt.close()

                    derivative_2D = [[0 for _ in range(len(one_derivative))] for _ in range(len(one_derivative))]

                    for loop_row in range(0, len(one_derivative)):
                        for loop_column in range(0, len(one_derivative)):
                            derivative_2D[loop_row][loop_column] = one_derivative[loop_column]

                    plt.imshow(derivative_2D)
                    plt.xticks([])
                    plt.yticks([])
                    plt.savefig(save_EDimg_addr + str(CNN_ED_imgnum) + '_' + str(i) + '.jpg')
                    plt.close()

                    img = Image.open(save_EDimg_addr + str(CNN_ED_imgnum) + '_' + str(i) + '.jpg')
                    img = img.resize((64, 64))
                    L = img.convert('L')
                    L.save(save_EDimg64_addr + str(CNN_ED_imgnum) + '.jpg')

                    max_index = evaluate_one_image(save_EDimg64_addr + str(CNN_ED_imgnum) + '.jpg',
                                                   'C:/Users/hao/Desktop/cnn_train/model_weight/ED_2D_makedata', 2)

                    print('max_index_ED: ', max_index, ' ', i)
                    if max_index == 1:
                        true_ED = 1
                        check_ED[i] = 1
                        print('!!!!!!!!!!!!!!! ')

                    CNN_ED_imgnum = CNN_ED_imgnum + 1

                fist_current_rms_period = 2
                break

        if true_ED == 1:  ### 有事件發生，進行特徵萃取


            V_i_next1, C_i_next1 = FIT_PS.fit_ps(voltage[(i + 1) * 1920:(i + 2) * 1920], current[(i + 1) * 1920:(i + 2) * 1920])  # 第 i+1 秒
            V_i, C_i = FIT_PS.fit_ps(voltage[i * 1920:(i + 1) * 1920], current[i * 1920:(i + 1) * 1920])  # 第 i 秒
            V_i_pre1, C_i_pre1 = FIT_PS.fit_ps(voltage[(i - 1) * 1920:i * 1920], current[(i - 1) * 1920:i * 1920])  # 第 i-1 秒


            if Power[i] - Power[i-1]<0:
                V_i_pre2, C_i_pre2 = FIT_PS.fit_ps(voltage[(i - 2) * 1920:(i - 1) * 1920],current[(i - 2) * 1920:(i - 1) * 1920])  # 第 i-2 秒
                C_2s = C_i_pre2 + C_i_pre1 + C_i + C_i_next1
            else:
                C_2s = C_i_pre1 + C_i + C_i_next1


            if Power[i] - Power[i-1] > 0:
                toscale_V=voltage[(i + 1) * 1920:(i + 2) * 1920]
                max_voltage = []  # 電壓正規化
                for loop_max in range(0, int(len(toscale_V) / 32)):
                    max_voltage.append(np.max(toscale_V[loop_max * 32:(loop_max + 1) * 32]))
                scale_voltage = stander_voltage / np.mean(max_voltage)
                C_2s = [x * scale_voltage for x in C_2s]
                img_C = C_2s[(loop_current_rms_period + 59 + 2) * 32:(loop_current_rms_period + 59 + 2) * 32 + img_size ** 2]
                print('on')

            else:
                toscale_V=voltage[(i - 2) * 1920:(i - 1) * 1920]
                max_voltage = []  # 電壓正規化
                for loop_max in range(0, int(len(toscale_V) / 32)):
                    max_voltage.append(np.max(toscale_V[loop_max * 32:(loop_max + 1) * 32]))
                scale_voltage = stander_voltage / np.mean(max_voltage)
                C_2s = [x * scale_voltage for x in C_2s]
                img_C = C_2s[(loop_current_rms_period + 59*2 - 2) * 32 - img_size ** 2:(loop_current_rms_period + 59*2 - 2) * 32]
                print('off')

            plt.plot(img_C)
            plt.savefig('C:/Users/hao/Desktop/cnn_train/grey/c/' + str(figure_num) + '.jpg')
            plt.close()

            new_img = []
            start_zero = 1
            while 1:
                if img_C[start_zero] > 0 and img_C[start_zero - 1] < 0:
                    for loop_scale in range(0, len(img_C)):
                        if loop_scale < len(img_C) - start_zero:
                            new_img.append(img_C[loop_scale + start_zero])
                        else:
                            new_img.append(0)
                    img_2D = [[0 for _ in range(img_size)] for _ in range(img_size)]
                    for row in range(0, img_size):
                        for col in range(0, img_size):
                            img_2D[row][col] = new_img[row * img_size + col]
                    plt.imshow(img_2D)
                    plt.xticks([])
                    plt.yticks([])
                    plt.savefig(save_img_addr + str(figure_num) + '.jpg')
                    plt.close()
                    figure_num = figure_num + 1
                    break
                else:
                    start_zero = start_zero + 1


        if i==299:

            plt.subplot(2, 1, 1)
            plt.plot(Power)
            plt.subplot(2, 1, 2)
            plt.plot(check_ED)
            plt.show()
            break
        else:
            i = i + 1




    # for img_name in os.listdir(save_img_addr):
    #     img_path = save_img_addr + img_name
    #     img = Image.open(img_path)
    #     img = img.resize((64, 64))
    #     L = img.convert('L')
    #     L.save(save_resizeimg_addr + str(img_name) + '.jpg')

def DTW(current):
    DTW_distance=[]
    zero_points=[]

    for loop_max in range(0, int(len(current)/32)-1):
        I_cycle1=current[loop_max * 32:(loop_max + 1) * 32]  ## 第一個週期
        I_cycle2=current[(loop_max + 1) * 32:(loop_max + 2) * 32]  ## 第二個週期
        distance, path = fastdtw(I_cycle1, I_cycle2, dist=euclidean)
        DTW_distance.append(distance)

    std_DTW=np.std(DTW_distance, ddof=1)
    # for loop_DTW_distance in range(0,len(DTW_distance)):

    return std_DTW

def evaluate_one_image(img_dir ,logs_train_dir ,N_CLASSES):

    # logs_train_dir='C:/Users/hao/Desktop/cnn_train/model_weight/ZV_DTW_SO'

    for img_name in range(0 ,1):
        img = Image.open(img_dir)
        imag = img.resize([64, 64])  # 由于图片在预处理阶段以及resize，因此该命令可略
        image_array = np.array(imag)
        # image_array = image
        with tf.Graph().as_default():
            BATCH_SIZE = 1
            # N_CLASSES = 3

            image = tf.cast(image_array, tf.float32)
            # image = tf.image.per_image_standardization(image)
            image = tf.reshape(image, [1, 64, 64, 1])
            # image = tf.reshape(image, [1, 64, 64, 3])

            logit = CNN_model.inference(image, BATCH_SIZE, N_CLASSES)
            logit = tf.nn.softmax(logit)
            x = tf.placeholder(tf.float32, shape=[64, 64])
            # x = tf.placeholder(tf.float32, shape=[64, 64, 3])
            saver = tf.train.Saver()

            with tf.Session() as sess:
                ckpt = tf.train.get_checkpoint_state(logs_train_dir)
                if ckpt and ckpt.model_checkpoint_path:
                    saver.restore(sess, ckpt.model_checkpoint_path)

                prediction = sess.run(logit, feed_dict={x: image_array})
                max_index =np.argmax(prediction)
    return  max_index



if __name__ == '__main__':
    run()