
#  特徵萃取  特徵：原始波形  取樣頻率：1920(CT) 加入FIT-PS固定周期  電壓過零點抓一個電流週期

import numpy as np
import matplotlib.pyplot as plt
import os
from PIL import Image
from scipy.spatial.distance import euclidean
from fastdtw import fastdtw

import RMS_transform as rms
import del_file as del_file
import FIT_PS

def run():

    filename_V = 'C:/Users/hao/Desktop/論文數據/O/O_1012_CT_1920_V.txt'  # 電壓數據位置
    filename_C = 'C:/Users/hao/Desktop/論文數據/O/O_1012_CT_1920_C.txt'  # 電流數據位置

    #  訓練數據：  S_0420_CT_1920_C  O_0420_CT_1920_C  I_0513_700w_CT_1920_C  I_0513_1100w_CT_1920_C  H_0513_CT_1920_V
    #             S_0522_CT_1920_C  O_0522_CT_1920_C  H_0522_CT_1920_C  I_0525_700w_CT_1920_C  I_0525_1100w_CT_1920_C   I_0527_700w_CT_1920_C   I_0527_1100w_CT_1920_C

    f = open(filename_V, 'r')
    line = f.read()
    list = line.split('\n')
    voltage = np.array(list, dtype=float)

    f = open(filename_C, 'r')
    line = f.read()
    list = line.split('\n')
    current = np.array(list, dtype=float)
    # current = [x * -1 for x in current]

    i = 1;

    P_threshold = 100;  # pc=80
    threshold = 1.5;
    img_size = 32;
    figure_num = 0;
    # stander_voltage=155

    save_img_addr = 'C:/Users/hao/Desktop/cnn_train/grey/1920/dd/'
    # save_resizeimg_addr = 'C:/Users/hao/Desktop/cnn_train/grey/1920/64/I_1100/'
    del_file.del_file(save_img_addr)
    # del_file.del_file(save_resizeimg_addr)
    del_file.del_file('C:/Users/hao/Desktop/cnn_train/grey/c/')

    Power = rms.RMS_transform(voltage, current)

    I_cycle = []
    array_img_C=[]

    check_ed=0
    # check_ed=[0]*len(Power)
    std_DTW_arr = [0]*len(Power)

    stander_voltage_arr = []  # 標準電壓
    for loop_max in range(0, int(5*1920 / 32)):
        stander_voltage_arr.append(np.max(voltage[loop_max * 32:(loop_max + 1) * 32]))
    stander_voltage=np.mean(stander_voltage_arr)
    print(stander_voltage)


    while 1:
        if np.abs(Power[i] - Power[i-1]) > P_threshold:

            ## (i-1)*2000,(i+2)*2000-1  三秒內電壓週期
            ## i*2000,(i+1)*2000-1+1000 事件發生一秒加半秒

            V_i_next1, C_i_next1 = FIT_PS.fit_ps(voltage[(i + 1) * 1920:(i + 2) * 1920], current[(i + 1) * 1920:(i + 2) * 1920])  # 第 i+1 秒
            V_i, C_i = FIT_PS.fit_ps(voltage[i * 1920:(i + 1) * 1920], current[i * 1920:(i + 1) * 1920])  # 第 i 秒
            V_i_pre1, C_i_pre1 = FIT_PS.fit_ps(voltage[(i - 1) * 1920:i * 1920], current[(i - 1) * 1920:i * 1920])  # 第 i-1 秒
            if Power[i] - Power[i-1]<0:
                V_i_pre2, C_i_pre2 = FIT_PS.fit_ps(voltage[(i - 2) * 1920:(i - 1) * 1920],current[(i - 2) * 1920:(i - 1) * 1920])  # 第 i-2 秒
                C_2s = C_i_pre2 + C_i_pre1 + C_i + C_i_next1
            else:
                C_2s = C_i_pre1 + C_i + C_i_next1

            # C_2s = C_i + C_i_next1

            if Power[i] - Power[i-1]>0:
                toscale_V=voltage[(i + 1) * 1920:(i + 2) * 1920]
                max_voltage = []  # 電壓正規化
                for loop_max in range(0, int(len(toscale_V) / 32)):
                    max_voltage.append(np.max(toscale_V[loop_max * 32:(loop_max + 1) * 32]))
                scale_voltage = stander_voltage / np.mean(max_voltage)
                C_2s = [x * scale_voltage for x in C_2s]
            else:
                toscale_V=voltage[(i - 2) * 1920:(i - 1) * 1920]
                max_voltage = []  # 電壓正規化
                for loop_max in range(0, int(len(toscale_V) / 32)):
                    max_voltage.append(np.max(toscale_V[loop_max * 32:(loop_max + 1) * 32]))
                scale_voltage = stander_voltage / np.mean(max_voltage)
                C_2s = [x * scale_voltage for x in C_2s]
            print(np.mean(max_voltage))


            plt.plot(C_2s)
            plt.savefig('C:/Users/hao/Desktop/cnn_train/grey/c/' + str(figure_num) + '.jpg')
            plt.close()

            for loop_cycle in range(0, int(len(C_2s) / 32)):
                val_to_mean = C_2s[loop_cycle * 32:(loop_cycle + 1) * 32]
                I_cycle_mean = np.mean(val_to_mean)
                signal_to_filter = [x - I_cycle_mean for x in
                                    C_2s[loop_cycle * 32:(loop_cycle + 1) * 32]]
                I_cycle.append(np.sum(np.abs(signal_to_filter)) / 32)

            fist_ed = 1
            for loop_I_cycle in range(0, len(I_cycle) - 1):
                delta_I = I_cycle[loop_I_cycle + 1] - I_cycle[loop_I_cycle]
                if np.abs(delta_I) > threshold:
                    if fist_ed==1:
                        # check_ed = check_ed+1
                        if Power[i] - Power[i-1] > 0:
                            img_C = C_2s[(loop_I_cycle+2)*32:(loop_I_cycle+2)*32 + img_size *10]

                        else:
                            img_C = C_2s[(loop_I_cycle-2)*32 - img_size *10:(loop_I_cycle-2)*32]

                        array_img_C.append(img_C)


                        new_img = []
                        for loop_img_C in range(1, len(img_C)):
                            if img_C[loop_img_C] > 0 and img_C[loop_img_C - 1] < 0:
                                for loop_scale in range(0, len(img_C)):
                                    if loop_scale < len(img_C) - loop_img_C:
                                        new_img.append(img_C[loop_scale + loop_img_C])
                                    else:
                                        new_img.append(0)
                                break
                        print('figure_num: ', figure_num, ' i: ', i, ' Power: ', Power[i] - Power[i-1], 'new_img: ',len(new_img),' loop_I_cycle:',loop_I_cycle)
                        # print('figure_num: ', figure_num, ' i: ', i, ' Power: ', Power[i] - Power[i - 1], 'new_img: ',len(new_img), ' loop_I_cycle:', loop_I_cycle)
                        plt.plot(img_C)
                        # plt.plot(new_img)
                        plt.ylim((-20, 20))
                        plt.savefig(save_img_addr + 'i_'+ str(i) + '_' + str(figure_num) + '.jpg')
                        plt.close()
                        fist_ed = fist_ed + 1
                        print(fist_ed)
                        figure_num=figure_num+1
                        check_ed=check_ed+1
                        break

        if (i + 1) * 1920 >= len(current) - 1920*5:
            print('finish')
            print(check_ed)
            # np.savetxt("C:/Users/hao/Desktop/cnn_train/grey/temp.txt", array_img_C, fmt='%f', delimiter=',')
            break
        else:
            i = i + 1
            I_cycle = []


    # for img_name in os.listdir(save_img_addr):
    #     img_path = save_img_addr + img_name
    #     img = Image.open(img_path)
    #     img = img.resize((64, 64))
    #     L = img.convert('L')
    #     L.save(save_resizeimg_addr + str(img_name) + '.jpg')

def DTW(voltage,current):
    DTW_distance=[]
    zero_points=[]
    for loop_cycle in range(0,len(current)-1):  # 電壓週期 1秒
        if voltage[loop_cycle + 1] > 0 and voltage[loop_cycle] < 0:
            zero_points.append(loop_cycle)

    for loop_zero_points in range(0, len(zero_points)-2):
        I_cycle1=current[zero_points[loop_zero_points]:zero_points[loop_zero_points + 1]]  ## 第一個週期
        I_cycle2=current[zero_points[loop_zero_points+1]:zero_points[loop_zero_points + 2]]  ## 第二個週期
        distance, path = fastdtw(I_cycle1, I_cycle2, dist=euclidean)
        DTW_distance.append(distance)

    std_DTW=np.std(DTW_distance, ddof=1)
    # for loop_DTW_distance in range(0,len(DTW_distance)):

    return std_DTW



if __name__ == '__main__':
    run()