

# 轉灰階論文 改事件檢測(mean) 電壓過零點抓一個電流週期  DTW判斷穩態 +THD

import numpy as np
import matplotlib.pyplot as plt
import os
from PIL import Image
from scipy.spatial.distance import euclidean
from fastdtw import fastdtw
import tensorflow as tf
from scipy.fftpack import fft,ifft

import RMS_transform as rms
import del_file as del_file
import CNN_model

def run():

    filename_V = 'C:/Users/hao/Desktop/數據/I_500w_CT_V.txt'  # 電壓數據位置
    filename_C = 'C:/Users/hao/Desktop/數據/I_500w_CT_C.txt'  # 電流數據位置

    # I_500w_CT_V  O_0213  H_0409_CT

    # filename_V = 'C:/Users/hao/Desktop/數據/demo_data/mixdata_MO_V.txt'  # 電壓數據位置
    # filename_C = 'C:/Users/hao/Desktop/數據/demo_data/mixdata_MO_C.txt'  # 電流數據位置

    f = open(filename_V, 'r')
    line = f.read()
    list = line.split('\n')
    voltage = np.array(list, dtype=float)

    f = open(filename_C, 'r')
    line = f.read()
    list = line.split('\n')
    current = np.array(list, dtype=float)

    i = 0;

    P_threshold = 100;
    threshold = 1;
    std_threshold=2;
    THD_threshod=15;
    img_size = 33;

    figure_num = 0;

    save_img_addr = 'C:/Users/hao/Desktop/cnn_train/grey/mix/'
    save_resizeimg_addr = 'C:/Users/hao/Desktop/cnn_train/grey/64/mix/'
    del_file.del_file(save_img_addr)
    del_file.del_file(save_resizeimg_addr)
    del_file.del_file('C:/Users/hao/Desktop/cnn_train/grey/c/')

    Power = rms.RMS_transform(voltage, current)

    zero_points = []
    I_cycle = []
    I_cycle_start = []
    # std_DTW_arr=[]
    status = []

    check_ed=[0]*len(Power)
    std_DTW_arr = [0]*len(Power)

    load_S = []
    load_O = []
    load_M = []
    load_H = []
    load_I = []

    status_S = 0
    status_O = 0
    status_M = 0
    status_H = 0
    status_I = 0

    while 1:
        fist_ed = 1
        if np.abs(Power[i + 1] - Power[i]) > P_threshold:

            ## (i-1)*2000,(i+2)*2000-1  三秒內電壓週期
            ## i*2000,(i+1)*2000-1+1000 事件發生一秒加半秒
            if Power[i + 1] - Power[i] > 0:
                for loop_cycle in range(i * 2000, (i + 2) * 2000):  # 電壓週期 1.5秒
                    if voltage[loop_cycle + 1] > 0 and voltage[loop_cycle] < 0:
                        zero_points.append(loop_cycle)
            else:
                for loop_cycle in range(i * 2000, (i + 2) * 2000):  # 電壓週期 -1.5~1.5秒
                    if voltage[loop_cycle + 1] > 0 and voltage[loop_cycle] < 0:
                        zero_points.append(loop_cycle)
            print(np.size(zero_points))

            for loop_zero_points in range(0, len(zero_points) - 1):
                val_to_mean = current[zero_points[loop_zero_points]:zero_points[loop_zero_points + 1]]  # ptyhon會自動扣一
                I_cycle_mean = np.mean(val_to_mean)
                signal_to_filter = [x - I_cycle_mean for x in
                                    current[zero_points[loop_zero_points]:zero_points[loop_zero_points + 1]]]
                I_cycle.append(np.sum(np.abs(signal_to_filter)) /
                               (zero_points[loop_zero_points + 1] - 1 - zero_points[loop_zero_points] + 1))
                I_cycle_start.append(zero_points[loop_zero_points])


            # fist_ed = 1
            for loop_I_cycle in range(0, len(I_cycle) - 1):
                delta_I = I_cycle[loop_I_cycle + 1] - I_cycle[loop_I_cycle]
                if np.abs(delta_I) > threshold:
                    if fist_ed==1:
                        check_ed[i] = 1
                        if Power[i + 1] - Power[i] > 0:
                            img_C = current[zero_points[loop_I_cycle + 1]:zero_points[loop_I_cycle + 1] + img_size ** 2]
                            img_V = voltage[zero_points[loop_I_cycle + 1]:zero_points[loop_I_cycle + 1] + img_size ** 2]
                            # img = current[(i + 1) * 2000:(i + 1) * 2000 + img_size ** 2]
                            # img_V = voltage[(i + 1) * 2000:(i + 1) * 2000 + img_size ** 2]
                            std_DTW_arr[i]=DTW(img_V, img_C)
                            to_thd=current[(i+2) * 2000:(i+3) * 2000]
                            # std_DTW_arr.append(DTW(img_V, img))
                            steady = 0;
                            while 1:
                                std_DTW = DTW(img_V, img_C)
                                if std_DTW > std_threshold:
                                    steady = steady + 500
                                    img_C = current[
                                          zero_points[loop_I_cycle + 1] + steady:
                                          zero_points[loop_I_cycle + 1] + img_size ** 2 + steady]
                                    img_V = voltage[
                                            zero_points[loop_I_cycle + 1] + steady:
                                            zero_points[loop_I_cycle + 1] + img_size ** 2 + steady]
                                    # img = current[(i + 1) * 2000 + steady: (i + 1) * 2000 + steady + img_size ** 2]
                                    # img_V = voltage[(i + 1) * 2000 + steady: (i + 1) * 2000 + steady + img_size ** 2]
                                else:
                                    break
                        else:
                            img_C = current[zero_points[loop_I_cycle + 1] - img_size ** 2:zero_points[loop_I_cycle + 1]]
                            img_V = voltage[zero_points[loop_I_cycle + 1] - img_size ** 2:zero_points[loop_I_cycle + 1]]
                            # img = current[i * 2000 - 1000 - img_size ** 2: i * 2000 - 1000]
                            # img_V = voltage[i * 2000 - 1000 - img_size ** 2: i * 2000 - 1000]
                            std_DTW_arr[i] = DTW(img_V, img_C)
                            to_thd = current[(i - 2) * 2000:(i-1) * 2000]
                            # std_DTW_arr.append(DTW(img_V, img))
                            steady = 0;
                            while 1:
                                std_DTW = DTW(img_V, img_C)
                                if std_DTW > std_threshold:
                                    steady = steady + 500
                                    img_C = current[
                                          zero_points[loop_I_cycle + 1] - img_size ** 2 - steady:
                                          zero_points[loop_I_cycle + 1] - steady]
                                    img_V = voltage[
                                            zero_points[loop_I_cycle + 1] - img_size ** 2 - steady:
                                            zero_points[loop_I_cycle + 1] - steady]
                                    # img = current[
                                    #       (i) * 2000 - 1000 - img_size ** 2 - steady: (i) * 2000 - 1000 - steady]
                                    # img_V = voltage[
                                    #         (i) * 2000 - 1000 - img_size ** 2 - steady: (i) * 2000 - 1000 - steady]
                                else:
                                    break

                        new_img = []
                        start_zero = 1

                        while 1:
                            if img_C[start_zero] > 0 and img_C[start_zero - 1] < 0:
                                for loop_scale in range(0, len(img_C)):
                                    if loop_scale < len(img_C) - start_zero:
                                        new_img.append(img_C[loop_scale + start_zero])
                                    else:
                                        new_img.append(0)
                                img_2D = [[0 for _ in range(img_size)] for _ in range(img_size)]
                                # img_2D=np.zeros((img_size,img_size))
                                for row in range(0, img_size):
                                    for col in range(0, img_size):
                                        img_2D[row][col] = new_img[row * img_size + col]
                                # print('figure_num: ', figure_num, ' i: ', i, ' ', Power[i + 1] - Power[i])
                                # plt.imshow(img_2D, cmap="gray")
                                plt.imshow(img_2D)
                                plt.xticks([])
                                plt.yticks([])
                                plt.savefig(save_img_addr + str(figure_num) + '.jpg')
                                plt.close()
                                img = Image.open(save_img_addr + str(figure_num) + '.jpg')
                                img = img.resize((64, 64))
                                L = img.convert('L')
                                L.save(save_resizeimg_addr + str(figure_num) + '.jpg')
                                # img.save(save_resizeimg_addr + str(figure_num) + '.jpg')

                                THD=thd(to_thd)
                                if THD>THD_threshod:
                                    max_index = evaluate_one_image(save_resizeimg_addr + str(figure_num) + '.jpg',
                                                                   'C:/Users/hao/Desktop/cnn_train/model_weight/ZV_DTW_M',1)
                                    max_index=max_index+4
                                else:
                                    max_index = evaluate_one_image(save_resizeimg_addr + str(figure_num) + '.jpg',
                                                                   'C:/Users/hao/Desktop/cnn_train/model_weight/ZV_OW_SOHI_CT',4)

                                print('figure_num: ', figure_num, ' i: ', i, ' ', Power[i + 1] - Power[i],'max_index ',max_index,' THD: ',THD)
                                status.append(max_index)
                                plt.plot(new_img)
                                plt.savefig('C:/Users/hao/Desktop/cnn_train/grey/c/' + str(figure_num) + '.jpg')
                                figure_num = figure_num + 1
                                plt.close()
                                break
                            else:
                                start_zero = start_zero + 1
                        fist_ed = fist_ed + 1
        else:
            std_DTW_arr[i]=0

        if (i + 1) * 2000 >= len(current) - 4000:
            plt.figure()
            plt.subplot(6, 1, 1)
            plt.ylabel('current(A)')
            plt.plot(current)
            plt.subplot(6, 1, 2)
            plt.ylabel('electric pot')
            plt.plot(load_S)
            plt.subplot(6, 1, 3)
            plt.ylabel('oven')
            plt.plot(load_O)
            plt.subplot(6, 1, 4)
            plt.xlabel('time(S)')
            plt.ylabel('microwave oven')
            plt.plot(load_M)
            plt.subplot(6, 1, 5)
            plt.xlabel('time(S)')
            plt.ylabel('hair dryer')
            plt.plot(load_H)
            plt.subplot(6, 1, 6)
            plt.xlabel('time(S)')
            plt.ylabel('Induction cooker')
            plt.plot(load_I)
            plt.show()
            print('finish')
            break
        else:
            if fist_ed > 1:
                max_index = max(status, key=status.count)
                print('status: ',status,'  max_index',max_index)
                print('////////////////////////////////////////////////////////////')
                if max_index == 0:
                    if Power[i + 1] - Power[i] > 0:
                        status_S = 1
                    else:
                        status_S = 0
                elif max_index == 1:

                    if Power[i + 1] - Power[i] > 0:
                        status_O = 1
                    else:
                        status_O = 0
                elif max_index == 4:
                    # load_M.append(status_on)
                    if Power[i + 1] - Power[i] > 0:
                        status_M = 1
                    else:
                        status_M = 0
                elif max_index == 2:
                    if Power[i + 1] - Power[i] > 0:
                        status_H = 1
                    else:
                        status_H = 0
                elif max_index == 3:
                    if Power[i + 1] - Power[i] > 0:
                        status_I = 1
                    else:
                        status_I = 0
            load_S.append(status_S)
            load_O.append(status_O)
            load_M.append(status_M)
            load_H.append(status_H)
            load_I.append(status_I)
            i = i + 1
            zero_points = []
            I_cycle = []
            I_cycle_start = []
            status = []

def DTW(voltage,current):
    DTW_distance=[]
    zero_points=[]
    for loop_cycle in range(0,len(current)-1):  # 電壓週期 1秒
        if voltage[loop_cycle + 1] > 0 and voltage[loop_cycle] < 0:
            zero_points.append(loop_cycle)

    for loop_zero_points in range(0, len(zero_points)-2):
        I_cycle1=current[zero_points[loop_zero_points]:zero_points[loop_zero_points + 1]]  ## 第一個週期
        I_cycle2=current[zero_points[loop_zero_points+1]:zero_points[loop_zero_points + 2]]  ## 第二個週期
        distance, path = fastdtw(I_cycle1, I_cycle2, dist=euclidean)
        DTW_distance.append(distance)

    std_DTW=np.std(DTW_distance, ddof=1)
    # for loop_DTW_distance in range(0,len(DTW_distance)):

    return std_DTW


def evaluate_one_image(img_dir,logs_train_dir,N_CLASSES):

    # logs_train_dir='C:/Users/hao/Desktop/cnn_train/model_weight/ZV_DTW_SO'

    for img_name in range(0,1):
        img = Image.open(img_dir)
        imag = img.resize([64, 64])  # 由于图片在预处理阶段以及resize，因此该命令可略
        image_array = np.array(imag)
        # image_array = image
        with tf.Graph().as_default():
            BATCH_SIZE = 1
            # N_CLASSES = 3

            image = tf.cast(image_array, tf.float32)
            # image = tf.image.per_image_standardization(image)
            image = tf.reshape(image, [1, 64, 64, 1])
            # image = tf.reshape(image, [1, 64, 64, 3])

            logit = CNN_model.inference(image, BATCH_SIZE, N_CLASSES)
            logit = tf.nn.softmax(logit)
            x = tf.placeholder(tf.float32, shape=[64, 64])
            # x = tf.placeholder(tf.float32, shape=[64, 64, 3])
            saver = tf.train.Saver()

            with tf.Session() as sess:
                ckpt = tf.train.get_checkpoint_state(logs_train_dir)
                if ckpt and ckpt.model_checkpoint_path:
                    saver.restore(sess, ckpt.model_checkpoint_path)

                prediction = sess.run(logit, feed_dict={x: image_array})
                max_index=np.argmax(prediction)
    return  max_index


def thd(sampled_data):
    abs_yf = np.abs(fft(sampled_data))
    abs_data=abs_yf[1:int(len(abs_yf) / 2)]
    # sq_sum=0.0
    sq_sum=abs_data[179]**2+abs_data[299]**2+abs_data[419]**2
    # for r in range( len(abs_data)):
    #    sq_sum = sq_sum + (abs_data[r])**2
    # sq_harmonics = sq_sum -abs_data[59]**2.0
    sq_harmonics=sq_sum**0.5
    thd = 100*sq_harmonics / abs_data[59]

    return thd

if __name__ == '__main__':
    run()