
#  特徵萃取  特徵：原始波形  取樣頻率：1920(CT) 加入FIT-PS固定周期  電壓過零點抓一個電流週期

import numpy as np
import matplotlib.pyplot as plt
import os
from PIL import Image
from scipy.spatial.distance import euclidean
from fastdtw import fastdtw

import RMS_transform as rms
import del_file as del_file
import FIT_PS

def run():

    filename_V = 'C:/Users/hao/Desktop/數據/air_0727_1hr_CT_1920_1_V.txt'  # 電壓數據位置
    filename_C = 'C:/Users/hao/Desktop/數據/air_0727_1hr_CT_1920_1_C.txt'  # 電流數據位置

    #  訓練數據：  pc11_0528_CT_1920_C   pc2_0528_CT_1920  M_0420_CT_1920_C   pcting1_CT_1920_C  pcting2_CT_1920_C
    #             I_0513_700w_CT_1920_C

    f = open(filename_V, 'r')
    line = f.read()
    list = line.split('\n')
    voltage = np.array(list, dtype=float)

    f = open(filename_C, 'r')
    line = f.read()
    list = line.split('\n')
    current = np.array(list, dtype=float)

    i = 1;

    P_threshold = 70;  # pc=80
    threshold = 0.5;
    img_size = 32;
    figure_num = 0;  #7


    save_img_addr = 'C:/Users/hao/Desktop/cnn_train/grey/1920/air/'
    save_resizeimg_addr = 'C:/Users/hao/Desktop/cnn_train/grey/1920/64/air/'
    # del_file.del_file(save_img_addr)
    # del_file.del_file(save_resizeimg_addr)
    del_file.del_file('C:/Users/hao/Desktop/cnn_train/grey/c/')

    Power = rms.RMS_transform(voltage, current)

    I_cycle = []

    check_ed=0

    stander_voltage_arr = []  # 標準電壓
    for loop_max in range(0, int(5*1920 / 32)):
        stander_voltage_arr.append(np.max(voltage[loop_max * 32:(loop_max + 1) * 32]))
    stander_voltage=np.mean(stander_voltage_arr)
    print(stander_voltage)


    while 1:
        black_mag = 0
        if np.abs(Power[i] - Power[i-1]) > P_threshold:
            if np.abs(Power[i-1] - Power[i-2]) > P_threshold and Power[i-1] - Power[i-2]>0:
                black_mag=1
            elif np.abs(Power[i-1] - Power[i-2]) > P_threshold and Power[i-1] - Power[i-2]<0:
                black_mag = -1

            print(' i: ', i, ' Power: ', Power[i] - Power[i - 1])
            ## (i-1)*2000,(i+2)*2000-1  三秒內電壓週期
            ## i*2000,(i+1)*2000-1+1000 事件發生一秒加半秒

            V_i_next1, C_i_next1 = FIT_PS.fit_ps(voltage[(i + 1) * 1920:(i + 2) * 1920], current[(i + 1) * 1920:(i + 2) * 1920])  # 第 i+1 秒
            V_i, C_i = FIT_PS.fit_ps(voltage[i * 1920:(i + 1) * 1920], current[i * 1920:(i + 1) * 1920])  # 第 i 秒
            V_i_pre1, C_i_pre1 = FIT_PS.fit_ps(voltage[(i - 1) * 1920:i * 1920], current[(i - 1) * 1920:i * 1920])  # 第 i-1 秒
            if Power[i] - Power[i-1]<0:
                V_i_pre2, C_i_pre2 = FIT_PS.fit_ps(voltage[(i - 2) * 1920:(i - 1) * 1920],current[(i - 2) * 1920:(i - 1) * 1920])  # 第 i-2 秒
                C_2s = C_i_pre2 + C_i_pre1 + C_i + C_i_next1
            else:
                C_2s = C_i_pre1 + C_i + C_i_next1

            # C_2s = C_i + C_i_next1

            if Power[i] - Power[i-1]>0:
                toscale_V=voltage[(i + 1) * 1920:(i + 2) * 1920]
                max_voltage = []  # 電壓正規化
                for loop_max in range(0, int(len(toscale_V) / 32)):
                    max_voltage.append(np.max(toscale_V[loop_max * 32:(loop_max + 1) * 32]))
                scale_voltage = stander_voltage / np.mean(max_voltage)
                C_2s = [x * scale_voltage for x in C_2s]
            else:
                toscale_V=voltage[(i - 2) * 1920:(i - 1) * 1920]
                max_voltage = []  # 電壓正規化
                for loop_max in range(0, int(len(toscale_V) / 32)):
                    max_voltage.append(np.max(toscale_V[loop_max * 32:(loop_max + 1) * 32]))
                scale_voltage = stander_voltage / np.mean(max_voltage)
                C_2s = [x * scale_voltage for x in C_2s]
            print(np.mean(max_voltage))


            plt.plot(C_2s)
            plt.savefig('C:/Users/hao/Desktop/cnn_train/grey/c/' + str(figure_num)+'_i_'+str(i) + '.jpg')
            plt.close()

            for loop_cycle in range(0, int(len(C_2s) / 32)):
                val_to_mean = C_2s[loop_cycle * 32:(loop_cycle + 1) * 32]
                I_cycle_mean = np.mean(val_to_mean)
                signal_to_filter = [x - I_cycle_mean for x in
                                    C_2s[loop_cycle * 32:(loop_cycle + 1) * 32]]
                I_cycle.append(np.sum(np.abs(signal_to_filter)) / 32)

            fist_ed = 1
            for loop_I_cycle in range(0, len(I_cycle) - 1):
                delta_I = I_cycle[loop_I_cycle + 1] - I_cycle[loop_I_cycle]
                if np.abs(delta_I) > threshold:

                    if fist_ed==1:
                        std = DTW(C_2s[(loop_I_cycle - 30) * 32:(loop_I_cycle + 30) * 32])
                        # std, path = fastdtw(C_2s[(loop_I_cycle - 30) * 32:(loop_I_cycle ) * 32], C_2s[(loop_I_cycle ) * 32:(loop_I_cycle + 30) * 32], dist=euclidean)
                        print('std: ',std,len(C_2s[(loop_I_cycle - 30) * 32:(loop_I_cycle + 30) * 32]))
                        check_ed = check_ed+1
                        if Power[i] - Power[i-1] > 0:
                            print('np.abs(delta_I): ', np.abs(delta_I), ' ON !!!')
                            img_C = C_2s[(loop_I_cycle+2)*32:(loop_I_cycle+2)*32 + img_size ** 2]

                        else:
                            if std > 2.0:
                                if black_mag == 1:
                                    print('np.abs(delta_I): ', np.abs(delta_I), ' ON !!!')
                                    img_C = C_2s[(loop_I_cycle + 2) * 32:(loop_I_cycle + 2) * 32 + img_size ** 2]
                                elif black_mag == 0:
                                    print('np.abs(delta_I): ', np.abs(delta_I), ' OFF !!!')
                                    img_C = C_2s[(loop_I_cycle - 2) * 32 - img_size ** 2:(loop_I_cycle - 2) * 32]
                            else:
                                fist_ed = 2



                        if fist_ed == 1:
                            plt.plot(img_C)
                            plt.savefig('C:/Users/hao/Desktop/cnn_train/grey/c/' + str(figure_num) + '.jpg')
                            plt.close()

                            new_img = []
                            start_zero = 1
                            while 1:
                                if img_C[start_zero] > 0 and img_C[start_zero - 1] < 0:
                                    for loop_scale in range(0, len(img_C)):
                                        if loop_scale < len(img_C) - start_zero:
                                            new_img.append(img_C[loop_scale + start_zero])
                                        else:
                                            new_img.append(0)
                                    img_2D = [[0 for _ in range(img_size)] for _ in range(img_size)]
                                    for row in range(0, img_size):
                                        for col in range(0, img_size):
                                            img_2D[row][col] = new_img[row * img_size + col]
                                    plt.imshow(img_2D)
                                    plt.xticks([])
                                    plt.yticks([])
                                    plt.savefig(save_img_addr + str(figure_num) + '.jpg')
                                    plt.close()
                                    break
                                else:
                                    start_zero = start_zero + 1

                            print('figure_num: ', figure_num, ' i: ', i, ' Power: ', Power[i] - Power[i - 1],
                                  'new_img: ', len(new_img), ' loop_I_cycle:', len(I_cycle), loop_I_cycle,
                                  ' check_ed: ', check_ed)
                            fist_ed = 2
                            print('//////////////////////////////////////////////////')
                            figure_num = figure_num + 1
                            # check_ed = check_ed + 1
                            break


        if (i + 1) * 1920 >= len(current) - 1920*5:
            print('finish')
            print('check_ed: ',check_ed)
            break
        else:
            i = i + 1
            I_cycle = []


    for img_name in os.listdir(save_img_addr):
        img_path = save_img_addr + img_name
        img = Image.open(img_path)
        img = img.resize((64, 64))
        L = img.convert('L')
        L.save(save_resizeimg_addr + str(img_name) + '.jpg')

def DTW(current):
    DTW_distance=[]
    zero_points=[]

    for loop_max in range(0, int(len(current)/32)-1):
        I_cycle1=current[loop_max * 32:(loop_max + 1) * 32]  ## 第一個週期
        I_cycle2=current[(loop_max + 1) * 32:(loop_max + 2) * 32]  ## 第二個週期
        distance, path = fastdtw(I_cycle1, I_cycle2, dist=euclidean)
        DTW_distance.append(distance)

    std_DTW=np.std(DTW_distance, ddof=1)
    # for loop_DTW_distance in range(0,len(DTW_distance)):

    return std_DTW



if __name__ == '__main__':
    run()