

# 轉灰階論文 改事件檢測(mean) 電壓過零點抓一個電流週期

import numpy as np
import matplotlib.pyplot as plt
import os
from PIL import Image
from collections import Counter
import tensorflow as tf
import CNN_model

import RMS_transform as rms
import del_file as del_file


def run():
    filename_V = 'C:/Users/hao/Desktop/數據/H_0306_V.txt'  # 電壓數據位置
    filename_C = 'C:/Users/hao/Desktop/數據/H_0306_C.txt'  # 電流數據位置

    # filename_V = 'C:/Users/hao/Desktop/數據/demo_data/mixdata_MO_V.txt'  # 電壓數據位置
    # filename_C = 'C:/Users/hao/Desktop/數據/demo_data/mixdata_MO_C.txt'  # 電流數據位置

    f = open(filename_V, 'r')
    line = f.read()
    list = line.split('\n')
    voltage = np.array(list, dtype=float)

    f = open(filename_C, 'r')
    line = f.read()
    list = line.split('\n')
    current = np.array(list, dtype=float)

    i = 0;

    P_threshold = 100;
    threshold = 2;
    img_size = 33;
    figure_num = 0;
    save_img_addr = 'C:/Users/hao/Desktop/cnn_train/grey/1/mix/'
    save_resizeimg_addr = 'C:/Users/hao/Desktop/cnn_train/grey/64/mix/'
    del_file.del_file(save_img_addr)
    del_file.del_file(save_resizeimg_addr)
    del_file.del_file('C:/Users/hao/Desktop/cnn_train/grey/c/')

    Power = rms.RMS_transform(voltage, current)

    zero_points = []
    I_cycle = []
    I_cycle_start = []
    status = []

    load_S = []
    load_O = []
    load_M = []
    load_H = []

    status_S = 0
    status_O = 0
    status_M = 0
    status_H = 0
    while 1:
        check = 0
        if np.abs(Power[i + 1] - Power[i]) > P_threshold:

            ## (i-1)*2000,(i+2)*2000-1  三秒內電壓週期
            ## i*2000,(i+1)*2000-1+1000 事件發生一秒加半秒
            if Power[i + 1] - Power[i] > 0:
                for loop_cycle in range(i * 2000, (i + 1) * 2000 - 1 + 1000):  # 電壓週期 1.5秒
                    if voltage[loop_cycle + 1] > 0 and voltage[loop_cycle] < 0:
                        zero_points.append(loop_cycle)
            else:
                for loop_cycle in range(i * 2000 - 500, (i + 1) * 2000 - 1 + 500):  # 電壓週期 -1.5~1.5秒
                    if voltage[loop_cycle + 1] > 0 and voltage[loop_cycle] < 0:
                        zero_points.append(loop_cycle)

            for loop_zero_points in range(0, len(zero_points) - 1):
                val_to_mean = current[zero_points[loop_zero_points]:zero_points[loop_zero_points + 1]]  # ptyhon會自動扣一
                I_cycle_mean = np.mean(val_to_mean)
                signal_to_filter = [x - I_cycle_mean for x in
                                    current[zero_points[loop_zero_points]:zero_points[loop_zero_points + 1]]]
                I_cycle.append(np.sum(np.abs(signal_to_filter)) /
                               (zero_points[loop_zero_points + 1] - 1 - zero_points[loop_zero_points] + 1))
                I_cycle_start.append(zero_points[loop_zero_points])

            for loop_I_cycle in range(0, len(I_cycle) - 1):
                delta_I = I_cycle[loop_I_cycle + 1] - I_cycle[loop_I_cycle]
                if np.abs(delta_I) > threshold:
                    check = 1
                    if Power[i + 1] - Power[i] > 0:
                        img = current[(i + 1) * 2000 - 1 + 1000:(i + 1) * 2000 - 1 + 1000 + img_size ** 2]
                    else:
                        img = current[i * 2000 - 500 - img_size ** 2:i * 2000 - 500]
                    new_img = []
                    start_zero = 1
                    while 1:
                        if img[start_zero] > 0 and img[start_zero - 1] < 0:
                            for loop_scale in range(0, len(img)):
                                if loop_scale < len(img) - start_zero:
                                    new_img.append(img[loop_scale + start_zero])
                                else:
                                    new_img.append(0)
                            img_2D = [[0 for _ in range(img_size)] for _ in range(img_size)]
                            # img_2D=np.zeros((img_size,img_size))
                            for row in range(0, img_size):
                                for col in range(0, img_size):
                                    img_2D[row][col] = new_img[row * img_size + col]
                            print(np.size(img_2D))
                            # plt.imshow(img_2D, cmap="gray")
                            plt.imshow(img_2D)
                            plt.savefig(save_img_addr + str(figure_num) + '.jpg')
                            plt.close()
                            img = Image.open(save_img_addr + str(figure_num) + '.jpg')
                            img = img.resize((64, 64))
                            L = img.convert('L')
                            L.save(save_resizeimg_addr + str(figure_num) + '.jpg')
                            max_index = evaluate_one_image(save_resizeimg_addr + str(figure_num) + '.jpg')
                            status.append(max_index)

                            figure_num = figure_num + 1
                            break
                        else:
                            start_zero = start_zero + 1

        if (i + 1) * 2000 >= len(current) - 4000:
            plt.figure()
            plt.subplot(5, 1, 1)
            plt.ylabel('current(A)')
            plt.plot(current)
            plt.subplot(5, 1, 2)
            plt.ylabel('electric pot')
            plt.plot(load_S)
            plt.subplot(5, 1, 3)
            plt.ylabel('oven')
            plt.plot(load_O)
            plt.subplot(5, 1, 4)
            plt.xlabel('time(S)')
            plt.ylabel('microwave oven')
            plt.plot(load_M)
            plt.subplot(5, 1, 5)
            plt.xlabel('time(S)')
            plt.ylabel('hair dryer')
            plt.plot(load_H)
            plt.show()
            print('finish')
            break
        else:
            if check == 1:
                max_index = max(status, key=status.count)
                if max_index == 0:
                    if Power[i + 1] - Power[i] > 0:
                        status_S = 1
                    else:
                        status_S = 0
                elif max_index == 1:

                    if Power[i + 1] - Power[i] > 0:
                        status_O = 1
                    else:
                        status_O = 0
                elif max_index == 2:
                    # load_M.append(status_on)
                    if Power[i + 1] - Power[i] > 0:
                        status_M = 1
                    else:
                        status_M = 0
                elif max_index == 3:
                    if Power[i + 1] - Power[i] > 0:
                        status_H = 1
                    else:
                        status_H = 0
            load_S.append(status_S)
            load_O.append(status_O)
            load_M.append(status_M)
            load_H.append(status_H)
            i = i + 1
            zero_points = []
            I_cycle = []
            I_cycle_start = []
            status = []


def evaluate_one_image(img_dir):

    logs_train_dir='C:/Users/hao/Desktop/cnn_train/model_weight/ZV_mean_4load'

    for img_name in range(0,1):
        img = Image.open(img_dir)
        imag = img.resize([64, 64])  # 由于图片在预处理阶段以及resize，因此该命令可略
        image_array = np.array(imag)
        # image_array = image
        with tf.Graph().as_default():
            BATCH_SIZE = 1
            N_CLASSES = 4

            image = tf.cast(image_array, tf.float32)
            # image = tf.image.per_image_standardization(image)
            image = tf.reshape(image, [1, 64, 64, 1])

            logit = CNN_model.inference(image, BATCH_SIZE, N_CLASSES)
            logit = tf.nn.softmax(logit)
            x = tf.placeholder(tf.float32, shape=[64, 64])
            saver = tf.train.Saver()

            with tf.Session() as sess:
                ckpt = tf.train.get_checkpoint_state(logs_train_dir)
                if ckpt and ckpt.model_checkpoint_path:
                    saver.restore(sess, ckpt.model_checkpoint_path)

                prediction = sess.run(logit, feed_dict={x: image_array})
                max_index=np.argmax(prediction)
    return  max_index

if __name__ == '__main__':
    run()



